//
//  ListRemove.swift
//  tmr
//
//  Created by Tomáš Geržičák on 25.05.2021.
//

import SwiftUI

struct ListRemove: View {
    @State private var numbers: [UserModel] = [
        UserModel.example
    ]
    @State private var currentNumber = 1
    
    var body: some View {
        VStack {
            List {
                ForEach(numbers, id: \.id) { user in
//                    Text("\($0)")
                    UserListItem(user: user)
                }.onDelete(perform: removeRows)
            }

//            Button("Add Number") {
//                self.numbers.append(self.currentNumber)
//                self.currentNumber += 1
//            }
        }
    }
    
    func removeRows(at offsets: IndexSet) {
        numbers.remove(atOffsets: offsets)
    }
}

struct ListRemove_Previews: PreviewProvider {
    static var previews: some View {
        ListRemove()
    }
}
