//
//  EditTask.swift
//  tmr
//
//  Created by Tomáš Geržičák on 22.05.2021.
//

import SwiftUI
import Combine

struct EditTask: View {
    @StateObject var viewModel: ViewModel
    @Binding var sheetOpened: Bool
    var disposeBag = Set<AnyCancellable>()
    var added: () -> Void
    var body: some View {
        VStack{
            List{
                Section(header: Text("Name")){
                    TextField("Name", text: $viewModel.name)
                }
                Section(header: Text("Description")){
                    TextEditor(text: $viewModel.description)
                }
                Section(header: Text("Deadline")){
                    if !viewModel.showDeadline {
                        Button("Add deadline"){
                            self.viewModel.showDeadline = true
                        }
                    } else {
                        DatePicker("Deadline", selection: $viewModel.deadline, displayedComponents: [.date])
                            .datePickerStyle(GraphicalDatePickerStyle())
                    }
                }
                Section(header: Text("Users")){
                    UsersList(users: $viewModel.users, addUserClicked: {
                        viewModel.addUserModalOpened = true
                    }) { userIndex in
                        
                    }
                }
            }.listStyle(InsetGroupedListStyle())
        }
        .navigationBarTitle("Add task", displayMode: .inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: {
                    self.sheetOpened = false
                }, label: {
                    Text("Back")
                })
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    if viewModel.validate() {
                        self.viewModel.editTask {
                            added()
                            self.sheetOpened = false
                        } error: { err in
                            debugPrint(err)
                        }
                    }
                }, label: {
                    Text("Save")
                })
            }
        })
        .sheet(isPresented: $viewModel.addUserModalOpened, content: {
            NavigationView{
                ChooseUser(users: viewModel.otherPossibleUsers, userChoosed: { user in
                    viewModel.users.append(user)
                }, sheetOpened: $viewModel.addUserModalOpened)
            }
        })
        .alert(isPresented: $viewModel.showErrorAlert, content: {
            Alert(title: Text("Validation error"), message: Text(viewModel.errorTitle))
        })
    }
}

extension EditTask {
    class ViewModel:ObservableObject {
        var task: TaskModel
        @Published var name: String = ""
        @Published var description: String = ""
        @Published var deadline: Date = Date()
        @Published var users: [UserModel] = []
        @Published var addUserModalOpened: Bool = false
        @Published var showErrorAlert: Bool = false
        @Published var showDeadline = false
        var disposeBag = Set<AnyCancellable>()
        var errorTitle = ""
        var otherPossibleUsers: [UserModel] {
            return allProjectUsers.filter { user in
                return !users.contains { userModel in
                    user.id == userModel.id
                }
            }
        }
        private var allProjectUsers: [UserModel]
        init(task: TaskModel) {
            self.task = task
            self.name = task.name
            self.description = task.description
            self.deadline = task.deadline ?? Date()
            self.users = task.users
            self.allProjectUsers = ProjectService.instance.load(from: task.projectId)?.users ?? []
            self.users.append(UserService.instance.getLoggedInUser())
        }
        
        func validate() -> Bool{
            if self.name.isEmpty {
                self.errorTitle = "Task name must not be empty"
                self.showErrorAlert = true
                return false
            }
            return true
        }
        
        func editTask(added: @escaping () -> Void, error: (_ err: Error) -> Void){
            let taskDto = self.buildTaskDto()
            TaskService.instance.edit(dto: taskDto, taskId: self.task.id)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tasks load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { _ in
                    added()
                    self.updateTaskModel()
                    
                }.store(in: &self.disposeBag)
        }
        
        func updateTaskModel() {
            self.task.name = self.name
            self.task.description = self.description
            self.task.deadline = self.deadline
        }
        
        func buildTaskDto() -> TaskDto{
            return TaskDto(name: self.name, deadline: self.deadline, description: self.description, projectId: task.projectId, columnId: task.columnId, users: users.map({ userModel -> Int64 in
                return userModel.id
            }))
        }
        
        func buildNewTask(newTaskId: Int64) -> TaskModel{
            return TaskModel(
                id: newTaskId,
                name: self.name,
                deadline: self.deadline,
                description: self.description,
                columnId: task.columnId,
                projectId: task.projectId,
                state: TaskState.IN_PROGRESS,
                users: self.users,
                comments: [],
                subtasks: [],
                trackedTimes: [])
        }
    }
}

struct EditTask_Previews: PreviewProvider {
    static var previews: some View {
        EditTask(viewModel: EditTask.ViewModel(task: TaskModel.example), sheetOpened: .constant(false)) {
            
        }
    }
}
