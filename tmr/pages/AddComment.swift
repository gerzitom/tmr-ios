//
//  AddComment.swift
//  tmr
//
//  Created by Tomáš Geržičák on 09.05.2021.
//

import SwiftUI

struct AddComment: View {
    @Binding var sheetOpened: Bool
    @State var text: String = ""
    @State var imagePickerOpened = false
    @State var useCamera = false
    @State var selectedImage: Image? = Image("")
    @State var newImages: [Int:Image] = [:]
    @State var index = 0
    var commentAdded: (_ newComment: CommentDto) -> Void
    var body: some View {
        Form{
            VStack(alignment: .leading){
                Text("Text")
                TextEditor(text: $text)
                    .background(Color.gray.opacity(0.1))
                ScrollView(.horizontal){
                    HStack{
                        ForEach(newImages.keys.sorted(), id: \.self){ key in
                            newImages[key]?.resizable().scaledToFit()
                        }
                    }
                    .frame(height: 200)
                }
//                .background(Color.pink)
                Button(action: {
                    imagePickerOpened = true
                    useCamera = false
                }, label: {
                    Text("Upload File")
                })
                .buttonStyle(PrimaryStyle())
                Button(action: {
                    imagePickerOpened = true
                    useCamera = true
                }, label: {
                    Text("Use camera")
                })
                .buttonStyle(SecondaryStyle())
                Spacer()
            }
        }
        .navigationBarTitle("Add comment", displayMode: .inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: {
                    self.sheetOpened = false
                }, label: {
                    Text("Back")
                })
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    self.sheetOpened = false
//                    let newComment = CommentDto(text: text, user: <#T##UserModel#>, created: Date())
//                    commentAdded()
                }, label: {
                    Text("Add")
                })
            }
        })
        .sheet(isPresented: $imagePickerOpened, content: {
            PhotoCaptureView(showImagePicker: self.$imagePickerOpened, image: self.$selectedImage, useCamera: useCamera)
//            ImagePicker(image: $selectedImage)
        })
        .onChange(of: selectedImage, perform: { value in
            if value != nil {
                newImages[index] = value
                index += 1
            }
        })
    }
}

struct AddComment_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AddComment(sheetOpened: .constant(true), commentAdded: { _ in
                
            })
        }
    }
}
