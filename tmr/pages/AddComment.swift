//
//  AddComment.swift
//  tmr
//
//  Created by Tomáš Geržičák on 09.05.2021.
//

import SwiftUI
import Combine

struct AddComment: View {
    @Binding var sheetOpened: Bool
    @StateObject var viewModel: ViewModel
    var commentAdded: (_ newComment: CommentModel) -> Void
    var body: some View {
        Form{
            VStack(alignment: .leading){
                Text("Text")
                TextEditor(text: $viewModel.text)
                    .background(Color.gray.opacity(0.1))
                viewModel.newImage?
                    .resizable()
                    .scaledToFit()
                    .frame(height: 200)
                
                
//                ScrollView(.horizontal){
//                    HStack{
//                        ForEach(newImages.keys.sorted(), id: \.self){ key in
//                            newImages[key]?.resizable().scaledToFit()
//                        }
//                    }
//                    .frame(height: 200)
//                }
//                .background(Color.pink)
                Button(action: {
                    viewModel.imagePickerOpened = true
                    viewModel.useCamera = false
                }, label: {
                    Text("Upload File")
                })
                .buttonStyle(PrimaryStyle())
                Button(action: {
                    viewModel.imagePickerOpened = true
                    viewModel.useCamera = true
                }, label: {
                    Text("Use camera")
                })
                .buttonStyle(SecondaryStyle())
                Spacer()
            }
        }
        .navigationBarTitle("Add comment", displayMode: .inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: {
                    self.sheetOpened = false
                }, label: {
                    Text("Back")
                })
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    self.sheetOpened = false
                    viewModel.addComment { comment in
                        commentAdded(comment)
                    } error: { err in
                        debugPrint(err)
                    }
                }, label: {
                    Text("Add")
                })
            }
        })
        .sheet(isPresented: $viewModel.imagePickerOpened, content: {
            PhotoCaptureView(showImagePicker: self.$viewModel.imagePickerOpened, image: self.$viewModel.selectedImage, useCamera: viewModel.useCamera)
        })
        .onChange(of: viewModel.selectedImage, perform: { value in
            viewModel.newImage = value
//            if value != nil {
//                newImages[index] = value
//                index += 1
//            }
        })
    }
}

extension AddComment {
    class ViewModel: ObservableObject{
        @Published var text: String = ""
        @Published var imagePickerOpened = false
        @Published var useCamera = false
        @Published var selectedImage: Image? = Image("")
        @Published var newImage: Image?
        @Published var index = 0
        var task: TaskModel
        
        var disposeBag = Set<AnyCancellable>()
        
        init(task: TaskModel) {
            self.task = task
        }
        
        func addComment(added: @escaping (_ newComment: CommentModel) -> Void, error: @escaping (_ err: Error) -> Void){
            TaskService.instance.addComment(dto: buildDto(), taskId: task.id)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Comment add finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        error(err)
                        break
                    }
                } receiveValue: { newCommentId in
                    TaskService.instance.getComment(commentId: newCommentId)
                        .sink { completion in
                            switch completion {
                            case .finished:
                                print("Comment add finished")
                                break
                            case .failure(let err):
                                debugPrint(err)
                                error(err)
                                break
                            }
                        } receiveValue: { comment in
                            added(comment)
                        }.store(in: &self.disposeBag)
                }.store(in: &disposeBag)
        }
        
        func buildDto() -> CommentDto{
            return CommentDto(text: self.text, userId: UserService.instance.getLoggedInUser().id, created: Date())
        }
        
        func buildComment(newCommentId: Int64) -> CommentModel{
            return CommentModel(id: newCommentId, text: self.text, user: UserService.instance.getLoggedInUser(), created: Date(), image: nil)
        }
    }
}

struct AddComment_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AddComment(sheetOpened: .constant(true), viewModel: AddComment.ViewModel(task: TaskModel.example), commentAdded: { _ in
                
            })
        }
    }
}
