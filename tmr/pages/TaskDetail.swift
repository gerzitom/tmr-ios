//
//  TaskDetail.swift
//  tmr
//
//  Created by Tomáš Geržičák on 08.05.2021.
//

import SwiftUI

struct TaskDetail: View {
    var model: TaskModel
    @State var editModalOpened = false
    var body: some View {
        VStack{
            Carousel(pages: [
                CarouselPage(content: AnyView(TaskDescription(model: model)), title: "Dashboard", id: 0),
                CarouselPage(content: AnyView(TaskSubtasks(subtasks: model.subtasks)), title: "Subtasks", id: 1),
                CarouselPage(content: AnyView(TaskTrackedTime(viewModel: TaskTrackedTime.ViewModel(model: model))), title: "Tracked time", id: 2),
                CarouselPage(content: AnyView(TaskComments(viewModel: TaskComments.ViewModel(model: model))), title: "Comments", id: 3)
            ])
        }
        .navigationBarTitle(model.name, displayMode: .large)
        .toolbar(content: {
            ToolbarItem(placement: .navigationBarLeading) {Text("")}
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    self.editModalOpened = true
                }, label: {
                    Text("Edit")
                })
            }
        })
        .sheet(isPresented: $editModalOpened, content: {
            NavigationView{
                EditTask(viewModel: EditTask.ViewModel(task: model), sheetOpened: $editModalOpened) {
                    //self.model = newTask
                }
            }
        })
    }
}


struct TaskDetail_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            TaskDetail(model: TaskModel.generateTask(name: "Create iOS app"), editModalOpened: false)
        }
    }
}
