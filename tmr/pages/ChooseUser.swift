//
//  ChooseUser.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import SwiftUI

struct ChooseUser: View {
    var users: [UserModel]
    var userChoosed: (_ choosenUser: UserModel) -> Void
    @Binding var sheetOpened: Bool
    var body: some View {
        VStack{
            List{
                ForEach(users){user in
                    Button(action: {
                        userChoosed(user)
                        self.sheetOpened = false
                    }, label: {
                        UserListItem(user: user)
                    })
                }
            }
        }
        .navigationBarTitle("Choose user", displayMode: .inline)
    }
}

struct ChooseUser_Previews: PreviewProvider {
    static var previews: some View {
        ChooseUser(users: UserModel.exampleArray, userChoosed: {_ in }, sheetOpened: .constant(false))
    }
}
