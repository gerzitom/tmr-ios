//
//  AddProject.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI
import Combine

struct AddProject: View {
    @StateObject var viewModel: ModelView
    @Binding var sheetOpened: Bool
    @State var addUserModalOpened = false
    @State var showAlert = false
    var projectAdded: (_ newProject: ProjectModel) -> Void
    var body: some View {
        Form{
            Section(header: Text("Project name")) {
                TextField("Project name", text: $viewModel.projectName)
            }
            Section(header: Text("Description")) {
                TextEditor(text: $viewModel.description)
            }
            DatePicker("Deadline", selection: $viewModel.deadline, displayedComponents: [.date])
            Section(header: Text("Project users")) {
                UsersList(users: $viewModel.users, addUserClicked: {
                    self.addUserModalOpened = true
                }) { userIndex in
                    
                }
            }
        }
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text("Validation error"), message: Text(viewModel.message))
        })
        .navigationBarTitle("Add project", displayMode: .inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: {
                    self.sheetOpened = false
                }, label: {
                    Text("Back")
                })
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    if viewModel.validate() {
                        viewModel.saveProject { newProject in
                            projectAdded(newProject)
//                            self.sheetOpened = false
                        } error: { err in
                            viewModel.message = err.localizedDescription
                            self.showAlert = true
                        }
                    } else {
                        self.showAlert = true
                    }
                }, label: {
                    Text("Save")
                })
            }
        })
        .sheet(isPresented: $addUserModalOpened, content: {
            NavigationView{
                ChooseUser(users: viewModel.otherPossibleUsers, userChoosed: { user in
                    viewModel.users.append(user)
                }, sheetOpened: $addUserModalOpened)
            }
        })
    }
}

extension AddProject{
    class ModelView: ObservableObject{
        var organization: OrganizationModel
        var message: String = ""
        @Published var projectName: String = ""
        @Published var description: String = ""
        @Published var deadline: Date = Date()
        @Published var users: [UserModel] = []
        var disposeBag = Set<AnyCancellable>()
        var otherPossibleUsers: [UserModel] {
            return organization.users.filter{
                let userId = $0.id
                if let _ = users.first(where: { $0.id == userId }){
                    return false
                } else {
                    return true
                }
            }
        }
        init(organization: OrganizationModel){
            self.organization = organization
            self.users.append(UserService.instance.getLoggedInUser())
        }
        
        func validate() -> Bool{
            if projectName.isEmpty {
                self.message = "Project name must not be empty"
                return false
            }
            if users.isEmpty {
                self.message = "Project must have at least one user"
                return false
            }
            return true
        }
        
        func buildDto() -> ProjectDto {
            return ProjectDto(name: self.projectName,organizationId: organization.id, users: self.users.map({ userModel -> Int64 in
                userModel.id
            }))
        }
        
        func buildNewProject(newProjectId: Int64) -> ProjectModel {
            return ProjectModel(id: newProjectId, name: self.projectName, users: self.users, organizationId: organization.id)
        }
        
        func saveProject(added: @escaping (_ newProject: ProjectModel) -> Void, error: @escaping (_ err: Error) -> Void){
            ProjectService.instance.add(newEntity: buildDto())
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Project load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        error(err)
                        break
                    }
                } receiveValue: { newProjectId in
                    added(self.buildNewProject(newProjectId: newProjectId))
                }.store(in: &disposeBag)

        }
        
        static var example = ModelView(organization: OrganizationModel.example)
    }
}

struct AddProject_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AddProject(viewModel: .example, sheetOpened: .constant(false), projectAdded: {_ in })
        }
    }
}
