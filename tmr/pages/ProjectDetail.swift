//
//  ProjectDetail.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI
import Combine

struct ProjectDetail: View {
    @StateObject var viewModel: ViewModel
    var pages: [CarouselPage] {
        var defaultPages = [
            CarouselPage(content: AnyView(ProjectDashboard()), title: "Dashboard", id: 0),
            CarouselPage(content: AnyView(Sprints()), title: "Sprints", id: 1),
        ]
        var index = 2
        for i in self.viewModel.columns.indices {
            defaultPages.append(CarouselPage(content: AnyView(Column(viewModel: Column.ViewModel(model: $viewModel.columns[i]))), title: viewModel.columns[i].name, id: index))
            index += 1
        }
        return defaultPages
    }
    var body: some View {
        VStack(alignment: .leading, spacing: nil){
            Carousel(pages: pages)
        }
        .navigationTitle("\(viewModel.model.name)")
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {}, label: {
                    Text("Edit")
                })
            }
        })
        .onAppear{
            viewModel.loadColumns()
        }
        .sheet(isPresented: $viewModel.editModalOpened, content: {
            /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Content@*/Text("Sheet Content")/*@END_MENU_TOKEN@*/
        })
    }
}

extension ProjectDetail{
    class ViewModel: ObservableObject {
        @Published var columns: [ColumnModel] = []
        @Published var editModalOpened = false
        @Binding var model: ProjectModel
        var disposeBag = Set<AnyCancellable>()
        init(model: Binding<ProjectModel>) {
            self._model = model
        }
        
        func loadColumns(){
            ColumnService.instance.getByProjectId(id: self.model.id)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Columns load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { columns in
                    DispatchQueue.main.async {
                        self.columns = columns.sorted(by: { $0.order < $1.order })
                    }
                }.store(in: &disposeBag)
        }
    }
}

struct ProjectDetail_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            ProjectDetail(viewModel: ProjectDetail.ViewModel(model: .constant(ProjectModel.example)))
        }
    }
}
