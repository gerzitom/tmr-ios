//
//  AddTask.swift
//  tmr
//
//  Created by Tomáš Geržičák on 21.05.2021.
//

import SwiftUI
import Combine

struct AddTask: View {
    @StateObject var viewModel: ViewModel
    @Binding var sheetOpened: Bool
    var disposeBag = Set<AnyCancellable>()
    var added: (_ newTask: TaskModel) -> Void
    var body: some View {
        VStack{
            List{
                Section(header: Text("Name")){
                    TextField("Name", text: $viewModel.name)
                }
                Section(header: Text("Description")){
                    TextEditor(text: $viewModel.description)
                }
                Section(header: Text("Deadline")){
                    DatePicker("Deadline", selection: $viewModel.deadline, displayedComponents: [.date])
                        .datePickerStyle(GraphicalDatePickerStyle())
                }
                Section(header: Text("Users")){
                    UsersList(users: viewModel.users, addUserClicked: {
                        viewModel.addUserModalOpened = true
                    })
                }
            }.listStyle(InsetGroupedListStyle())
        }
        .navigationBarTitle("Add task", displayMode: .inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: {
                    self.sheetOpened = false
                }, label: {
                    Text("Back")
                })
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    if viewModel.validate() {
                        self.viewModel.saveTask { newTask in
                            added(newTask)
                            self.sheetOpened = false
                        } error: { err in
                            debugPrint(err)
                        }
                    }
                }, label: {
                    Text("Save")
                })
            }
        })
        .sheet(isPresented: $viewModel.addUserModalOpened, content: {
            NavigationView{
                ChooseUser(users: viewModel.otherPossibleUsers, userChoosed: { user in
                    viewModel.users.append(user)
                }, sheetOpened: $viewModel.addUserModalOpened)
            }
        })
        .alert(isPresented: $viewModel.showErrorAlert, content: {
            Alert(title: Text("Validation error"), message: Text(viewModel.errorTitle))
        })
    }
}

extension AddTask {
    class ViewModel:ObservableObject {
        var column: ColumnModel
        @Published var name: String = ""
        @Published var description: String = ""
        @Published var deadline: Date = Date()
        @Published var users: [UserModel] = []
        @Published var addUserModalOpened: Bool = false
        @Published var showErrorAlert: Bool = false
        var disposeBag = Set<AnyCancellable>()
        var errorTitle = ""
        var otherPossibleUsers: [UserModel] {
            return allProjectUsers.filter { user in
                return !users.contains { userModel in
                    user.id == userModel.id
                }
            }
        }
        private var allProjectUsers: [UserModel]
        init(column: ColumnModel) {
            self.column = column
            self.allProjectUsers = ProjectService.instance.load(from: column.projectId)?.users ?? []
            self.users.append(UserService.instance.getLoggedInUser())
        }
        
        func validate() -> Bool{
            if self.name.isEmpty {
                self.errorTitle = "Task name must not be empty"
                self.showErrorAlert = true
                return false
            }
            return true
        }
        
        func saveTask(added: @escaping (_ newTask: TaskModel) -> Void, error: (_ err: Error) -> Void){
            TaskService.instance.add(dto: self.buildTaskDto())
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tasks load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { newTaskId in
                    added(self.buildNewTask(newTaskId: newTaskId))
                }.store(in: &self.disposeBag)
        }
        
        func buildTaskDto() -> TaskDto{
            return TaskDto(name: self.name, deadline: self.deadline, description: self.description, projectId: column.projectId, columnId: column.id, users: users.map({ userModel -> Int64 in
                return userModel.id
            }))
        }
        
        func buildNewTask(newTaskId: Int64) -> TaskModel{
            return TaskModel(
                id: newTaskId,
                name: self.name,
                deadline: self.deadline,
                description: self.description,
                columnId: self.column.id,
                projectId: self.column.projectId,
                state: TaskState.IN_PROGRESS,
                users: self.users,
                comments: [],
                subtasks: [],
                trackedTimes: [])
        }
    }
}

struct AddTask_Previews: PreviewProvider {
    static var previews: some View {
        AddTask(viewModel: AddTask.ViewModel(column: ColumnModel.example), sheetOpened: .constant(false)) { _ in
            
        }
    }
}
