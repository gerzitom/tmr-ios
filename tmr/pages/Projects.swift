//
//  Projects.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI

struct Projects: View {
    @Binding var organization: OrganizationModel
    @State var sheetOpened = false
    var body: some View {
        VStack{
            ScrollView{
                ForEach(organization.projects.indices, id: \.self){ index in
                    NavigationLink(
                        destination: ProjectDetail(viewModel: ProjectDetail.ViewModel(model: $organization.projects[index])),
                        label: {
                            Project(viewModel: Project.ViewModel(projectModel: organization.projects[index]))
                        })
                }
                .padding(.horizontal)
                .padding(.top)
            }
            Spacer()
        }
        .navigationTitle("Projects")
        .toolbar{
            ToolbarItem(placement:.navigationBarTrailing, content: {
                Label("Plus", systemImage: "plus")
                    .padding(.all, 10)
                    .background(Color.primaryColor)
                    .foregroundColor(.black)
                    .cornerRadius(50)
                    .onTapGesture {
                        sheetOpened = true
                    }
            })
        }
        .sheet(isPresented: $sheetOpened, content: {
            NavigationView{
                let modelView = AddProject.ModelView(organization: organization)
                AddProject(
                    viewModel: modelView,
                    sheetOpened: $sheetOpened,
                    projectAdded: { project in
                        organization.projects.append(project)
                        sheetOpened = false
                    })
            }
        })
    }
}


struct Projects_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            Projects(organization: .constant(.example))
        }
    }
}
