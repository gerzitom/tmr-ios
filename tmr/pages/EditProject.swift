//
//  EditProject.swift
//  tmr
//
//  Created by Tomáš Geržičák on 22.05.2021.
//

import SwiftUI
import Combine

struct EditProject: View {
    @StateObject var viewModel: ViewModel
    @Binding var sheetOpened: Bool
    var disposeBag = Set<AnyCancellable>()
    var modified: (_ project: ProjectModel) -> Void
    var body: some View {
        VStack{
            List{
                Section(header: Text("Name")){
                    TextField("Name", text: $viewModel.name)
                }
                Section(header: Text("Users")){
                    UsersList(users: viewModel.users, addUserClicked: {
                        viewModel.addUserModalOpened = true
                    })
                }
            }.listStyle(InsetGroupedListStyle())
        }
        .navigationBarTitle("Add task", displayMode: .inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: {
                    self.sheetOpened = false
                }, label: {
                    Text("Back")
                })
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    if viewModel.validate() {
                        self.viewModel.editProject {
                            modified(viewModel.buildProject())
                            self.sheetOpened = false
                        } error: { err in
                            debugPrint(err)
                            self.viewModel.errorTitle = err.localizedDescription
                            self.viewModel.showErrorAlert = true
                        }
                    }
                }, label: {
                    Text("Save")
                })
            }
        })
        .sheet(isPresented: $viewModel.addUserModalOpened, content: {
            NavigationView{
                ChooseUser(users: viewModel.otherPossibleUsers, userChoosed: { user in
                    viewModel.users.append(user)
                }, sheetOpened: $viewModel.addUserModalOpened)
            }
        })
        .alert(isPresented: $viewModel.showErrorAlert, content: {
            Alert(title: Text("Validation error"), message: Text(viewModel.errorTitle))
        })
    }
}

extension EditProject {
    class ViewModel:ObservableObject {
        var project: ProjectModel
        @Published var name: String = ""
        @Published var users: [UserModel] = []
        @Published var addUserModalOpened: Bool = false
        @Published var showErrorAlert: Bool = false
        @Published var showDeadline = false
        var disposeBag = Set<AnyCancellable>()
        var errorTitle = ""
        var otherPossibleUsers: [UserModel] {
            return allProjectUsers.filter { user in
                return !users.contains { userModel in
                    user.id == userModel.id
                }
            }
        }
        private var allProjectUsers: [UserModel]
        init(project: ProjectModel) {
            self.project = project
            self.name = project.name
            self.users = project.users
            self.allProjectUsers = ProjectService.instance.load(from: project.id)?.users ?? []
            self.users.append(UserService.instance.getLoggedInUser())
        }
        
        func validate() -> Bool{
            if self.name.isEmpty {
                self.errorTitle = "Task name must not be empty"
                self.showErrorAlert = true
                return false
            }
            return true
        }
        
        func editProject(added: @escaping () -> Void, error: (_ err: Error) -> Void){
            ProjectService.instance.edit(dto: self.buildProjectDto())
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tasks load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { newProjectId in
                    added()
                }.store(in: &self.disposeBag)
        }
        
        func buildProjectDto() -> ProjectUpdateDto{
            return ProjectUpdateDto(name: self.name)
        }
        
        func buildProject() -> ProjectModel{
            return ProjectModel(id: project.id, name: self.name, users: self.users, organizationId: project.organizationId)
        }
    }
}

struct EditProject_Previews: PreviewProvider {
    static var previews: some View {
        EditProject(viewModel: EditProject.ViewModel(project: ProjectModel.example), sheetOpened: .constant(false)) { _ in
            
        }
    }
}

