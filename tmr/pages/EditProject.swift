//
//  EditProject.swift
//  tmr
//
//  Created by Tomáš Geržičák on 22.05.2021.
//

import SwiftUI
import Combine

struct EditProject: View {
    @StateObject var viewModel: ViewModel
    @Binding var sheetOpened: Bool
    var modified: (_ project: ProjectUpdateDto) -> Void
    var disposeBag = Set<AnyCancellable>()
    var body: some View {
        VStack(spacing: 0){
            List{
                Section(header: Text("Name")){
                    TextField("Name", text: $viewModel.name)
                }
            }.listStyle(InsetGroupedListStyle())
            UsersList(users: $viewModel.project.users, addUserClicked: {
                viewModel.addUserModalOpened = true
            }) { userId in
                viewModel.removeUser(userId: userId) {
                    debugPrint("USER REMOVED")
                } error: { error in
                    debugPrint(error)
                }
            }
            Spacer()
        }
        .navigationBarTitle("Edit project", displayMode: .inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading) {
                Button(action: {
                    self.sheetOpened = false
                }, label: {
                    Text("Back")
                })
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
                Button(action: {
                    if viewModel.validate() {
                        self.viewModel.editProject {editedProject in
                            modified(editedProject)
                            self.sheetOpened = false
                        } error: { err in
                            debugPrint(err)
                            self.viewModel.errorTitle = err.localizedDescription
                            self.viewModel.showErrorAlert = true
                        }
                    }
                }, label: {
                    Text("Save")
                })
            }
        })
        .sheet(isPresented: $viewModel.addUserModalOpened, content: {
            NavigationView{
                ChooseUser(users: viewModel.otherPossibleUsers, userChoosed: { user in
                    viewModel.addUser(userId: user.id) {
                        viewModel.project.users.append(user)
                    } error: { error in
                        viewModel.showErrorAlert = true
                        viewModel.errorTitle = error.localizedDescription
                    }

                }, sheetOpened: $viewModel.addUserModalOpened)
            }
        })
        .alert(isPresented: $viewModel.showErrorAlert, content: {
            Alert(title: Text("Validation error"), message: Text(viewModel.errorTitle))
        })
    }
}

extension EditProject {
    class ViewModel:ObservableObject {
        @Published var project: ProjectModel
        @Published var name: String = ""
        @Published var addUserModalOpened: Bool = false
        @Published var showErrorAlert: Bool = false
        @Published var showDeadline = false
        var disposeBag = Set<AnyCancellable>()
        var errorTitle = ""
        var otherPossibleUsers: [UserModel] {
            return allProjectUsers.filter { user in
                return !project.users.contains { userModel in
                    user.id == userModel.id
                }
            }
        }
        private var allProjectUsers: [UserModel]
        init(project: ProjectModel) {
            self.project = project
            self.name = project.name
            self.allProjectUsers = OrganizationsService.instance.loadOrganization(from: project.organizationId)?.users ?? []
        }
        
        func validate() -> Bool{
            if self.name.isEmpty {
                self.errorTitle = "Task name must not be empty"
                self.showErrorAlert = true
                return false
            }
            return true
        }
        
        func editProject(edited: @escaping (_ editedProject: ProjectUpdateDto) -> Void, error: (_ err: Error) -> Void){
            ProjectService.instance.edit(dto: self.buildProjectDto(), projectId: project.id)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tasks load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { newProjectId in
                    edited(self.buildProjectDto())
                }.store(in: &self.disposeBag)
        }
        
        func removeUser(userId: Int64, done: @escaping () -> Void, error: @escaping (_ err: Error) -> Void){
            ProjectService.instance.removeUser(projectId: project.id, userId: userId)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("User removed")
                        done()
                        break
                    case .failure(let err):
                        debugPrint(err)
                        error(err)
                        break
                    }
                } receiveValue: { _ in
                }.store(in: &self.disposeBag)
        }
        
        func addUser(userId: Int64, done: @escaping () -> Void, error: @escaping (_ err: Error) -> Void){
            ProjectService.instance.addUser(userProject: UserProjectDto(user: userId, project: project.id))
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("User removed")
                        done()
                        break
                    case .failure(let err):
                        debugPrint(err)
                        error(err)
                        break
                    }
                } receiveValue: { _ in
                }.store(in: &self.disposeBag)
        }
        
        
        
        func buildProjectDto() -> ProjectUpdateDto{
            return ProjectUpdateDto(name: self.name)
        }
    }
}

struct EditProject_Previews: PreviewProvider {
    static var previews: some View {
        EditProject(viewModel: EditProject.ViewModel(project: ProjectModel.example), sheetOpened: .constant(false)) { _ in
            
        }
    }
}

