//
//  WebsocketTest.swift
//  tmr
//
//  Created by Tomáš Geržičák on 23.05.2021.
//

import SwiftUI
import Starscream

struct WebsocketTest: View {
    private var webSocket: WebSocketWrapper = WebSocketWrapper()

    var body: some View {
        Text("Hello, World websockets!")
    }
}

extension WebsocketTest{
    class WebSocketWrapper: ObservableObject, WebSocketDelegate{

        var socket: WebSocket
        init(){
            var request = URLRequest(url: URL(string: "ws://localhost:8091/websocket")!)
            request.timeoutInterval = 5
            socket = WebSocket(request: request)
            socket.delegate = self
            socket.connect()
        }

        func didReceive(event: WebSocketEvent, client: WebSocket) {
            debugPrint(event)
        }
    }
}

struct WebsocketTest_Previews: PreviewProvider {
    static var previews: some View {
        WebsocketTest()
    }
}
