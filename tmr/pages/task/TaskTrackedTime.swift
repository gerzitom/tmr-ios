//
//  TaskTrackedTime.swift
//  tmr
//
//  Created by Tomáš Geržičák on 08.05.2021.
//

import SwiftUI
import Combine

struct TaskTrackedTime: View {
    @StateObject var viewModel: ViewModel
    var body: some View {
        VStack(alignment: .leading){
            TrackedTimeTimer(
                viewModel: TrackedTimeTimer.ViewModel(model: viewModel.model, pageViewModel: viewModel))
                .padding(.horizontal)
            Text("Tracked times log")
                .bold()
                .padding(.top)
                .font(.subheadline)
                .padding(.horizontal)
            List{
                ForEach(viewModel.trackedTimes){trackedTime in
                    UserTrackedTimeList(model: trackedTime)
                }
            }
        }.task {
            viewModel.loadTrackedTimes()
        }
    }
}

extension TaskTrackedTime {
    class ViewModel: ObservableObject {
        var model: TaskModel
        @Published var trackedTimes: [TrackedTimeModel] = []
        private var disposeBag = Set<AnyCancellable>()
        
        init(model: TaskModel) {
            self.model = model
        }
        
        func loadTrackedTimes() {
            TaskService.instance.getTrackedTimes(taskId: model.id)
                .sink { completion in
                    switch completion {
                    case .finished:
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { trackedTimes in
                    self.trackedTimes = trackedTimes
                }.store(in: &disposeBag)

        }
    }
}

struct TrackedTimeTimer: View {
    @StateObject var viewModel: ViewModel
    
    @State private var timer = Timer.publish(every: 1, on: .main, in: .common)
    @State private var timerSubscription: Cancellable?
    
    @State var time: Int = 0
    @State var started: Bool = false
    
    @State var errorAlertVisible = false
    @State var errorMessage: String = ""
    
    var formatedTime: String {
        let seconds = time % 60
        let secondsString = seconds < 10 ? "0" + "\(seconds)" : "\(seconds)"
        return "\(time / 60):"+secondsString
    }
    var body: some View {
        HStack{
            VStack(alignment: .leading){
                Text("Total tracked time")
                    .font(.system(size: 13))
                    .foregroundColor(.tmrGrey)
                HStack(spacing: 0){
                    Text(viewModel.totalTrackedTime)
                        .bold()
                        .font(.system(size: 33))
                        .foregroundColor(.primaryColor)
                }
            }
            Spacer()
            Button(action: {
                withAnimation {
                    if !self.started {
                        viewModel.startTracking {
                            self.timerSubscription = self.timer.connect()
                            self.started = true
                        } error: { err in
                            self.errorAlertVisible = true
                            self.errorMessage = err.localizedDescription
                        }
                    } else {
                        viewModel.endTracking {
                            self.timerSubscription?.cancel()
                            self.time = 0
                            self.started = false
                            self.viewModel.pageViewModel.loadTrackedTimes()
                        }
                    }
                }
            }, label: {
                HStack{
                    if started {
                        Text("\(formatedTime)")
                            .padding(.leading)
                            .onReceive(timer, perform: { _ in
                                time += 1
                            })
                    }
                    Image(systemName: started ? "stop.fill" : "play.fill" )
                        .frame(width: 46, height: 46, alignment: .center)
                }
                    .background(Color.primaryColor)
                    .foregroundColor(.black)
                    .cornerRadius(30)
            })
        }
        .alert(isPresented: $errorAlertVisible, content: {
            Alert(title: Text("Error"), message: Text(errorMessage))
        })
        .refreshable {
            //await pageViewModel.reload()
        }
    }
}

extension TrackedTimeTimer {
    class ViewModel: ObservableObject {
        var model: TaskModel
        var pageViewModel: TaskTrackedTime.ViewModel
        private var disposeBag = Set<AnyCancellable>()
        private var trackedTimeId: Int64?
        
        var totalTrackedTime: String{
            let totalTime = self.pageViewModel.trackedTimes.reduce(0, {result, model in
                result + model.elapsedTime
            })
            debugPrint(totalTime)
            return TrackedTimeModel.formatTime(elapsedTime: totalTime)
        }
        
        init(model: TaskModel, pageViewModel: TaskTrackedTime.ViewModel) {
            self.model = model
            self.pageViewModel = pageViewModel
        }
        
        func startTracking(done: @escaping () -> Void, error: @escaping  (_ error: Error) -> Void){
            TaskService.instance.startTrackTime(dto: buildDto())
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tracked time started")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        error(err)
                        break
                    }
                } receiveValue: { trackedTimeId in
                    self.trackedTimeId = trackedTimeId
                    done()
                }.store(in: &disposeBag)
        }
        
        func endTracking(done: @escaping () -> Void){
            TaskService.instance.endTrackTime(trackedTimeId: trackedTimeId!)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tracked time ended")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { _ in
                    done()
                }.store(in: &disposeBag)
        }
        
        func buildDto() -> TrackedTimeDto{
            return TrackedTimeDto(userId: UserService.instance.getLoggedInUser().id, taskId: model.id)
        }
    }
}

struct TaskTrackedTime_Previews: PreviewProvider {
    static var previews: some View {
        TaskTrackedTime(viewModel: TaskTrackedTime.ViewModel(model: TaskModel.example))
    }
}
