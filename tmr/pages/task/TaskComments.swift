//
//  TaskComments.swift
//  tmr
//
//  Created by Tomáš Geržičák on 08.05.2021.
//

import SwiftUI
import Combine

struct TaskComments: View {
    @State var modalOpened = false
    @StateObject var viewModel: ViewModel
    var body: some View {
        VStack {
            if viewModel.commentsLoading {
                Text("Loading")
            }
            ScrollView{
                VStack{
                    ForEach(viewModel.comments){comment in
                        Comment(model: comment)
                    }
                }
                .padding(.horizontal)
            }
            Button(action: {
                modalOpened = true
            }, label: {
                Text("Add comment")
            })
            .buttonStyle(PrimaryStyle(fullWidth: true))
            .padding(.horizontal)
            .padding(.top)
        }
        .sheet(isPresented: $modalOpened, content: {
            NavigationView{
                AddComment(sheetOpened: $modalOpened, viewModel: AddComment.ViewModel(task: viewModel.model)) { (comment) in
                    viewModel.comments.insert(comment, at: 0)
                }
            }
        })
        .onAppear{
            self.viewModel.loadComments()
        }
    }
}

extension TaskComments {
    class ViewModel: ObservableObject {
        var model: TaskModel
        var disposeBag = Set<AnyCancellable>()
        @Published var comments: [CommentModel] = []
        @Published var commentsLoading = true
        init(model: TaskModel){
            self.model = model
        }
        
        func loadComments(){
            TaskService.instance.getComments(taskId: model.id)
                .sink { completion in
                    self.commentsLoading = false
                    switch completion {
                    case .finished:
                        print("Comments load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { comments in
                    DispatchQueue.main.async {
                        self.comments = comments
                    }
                }.store(in: &disposeBag)

        }
    }
}

struct TaskComments_Previews: PreviewProvider {
    static var previews: some View {
        TaskComments(viewModel: TaskComments.ViewModel(model: TaskModel.example))
    }
}
