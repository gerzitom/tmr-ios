//
//  TaskSubtasks.swift
//  tmr
//
//  Created by Tomáš Geržičák on 08.05.2021.
//

import SwiftUI

struct TaskSubtasks: View {
    var subtasks: [TaskModel]
    var tasksInProgress: [TaskModel] {
        return subtasks.filter { task in
            task.state != .DONE
        }
    }
    var body: some View {
        VStack{
            ForEach(tasksInProgress){ task in
                NavigationLink(
                    destination: TaskDetail(model: task),
                    label: {
                        Task(model: task)
                    })
            }
        }
        .padding(.horizontal)
    }
}

struct TaskSubtasks_Previews: PreviewProvider {
    static var previews: some View {
        TaskSubtasks(subtasks: TaskModel.exampleArray)
    }
}
