//
//  TaskDashboard.swift
//  tmr
//
//  Created by Tomáš Geržičák on 08.05.2021.
//

import SwiftUI

struct TaskDescription: View {
    var model: TaskModel
    var body: some View {
        VStack(alignment: .leading, spacing: 0){
            Text("\(model.description)")
        }
    }
}

struct TaskDashboard_Previews: PreviewProvider {
    static var previews: some View {
        TaskDescription(model: TaskModel.example)
    }
}
