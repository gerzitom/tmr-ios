//
//  ProjectTasks.swift
//  tmr
//
//  Created by Tomáš Geržičák on 30.04.2021.
//

import SwiftUI
import Combine

struct Column: View {
    @StateObject var viewModel: ViewModel
    var body: some View {
        VStack{
            ForEach(viewModel.tasksInProgress){task in
                NavigationLink(
                    destination: TaskDetail(model: task),
                    label: {
                        Task(model: task)
                    })
            }
            if viewModel.tasksInProgress.count == 0 {
                Text("There are no tasks in progress")
            }
            Spacer()
            Button(action: {
                self.viewModel.sheetOpened = true
            }, label: {
                Text("Add new task")
            })
            .buttonStyle(SecondaryStyle(fullWidth: true))
            .padding(.top, 20)
        }
        .padding(.horizontal)
        .onAppear{
            self.viewModel.loadTasks()
        }
        .sheet(isPresented: $viewModel.sheetOpened, content: {
            NavigationView{
                AddTask(viewModel: AddTask.ViewModel(column: viewModel.model), sheetOpened: $viewModel.sheetOpened) { newTask in
                    viewModel.tasks.append(newTask)
                }
            }
        })
    }
}

extension Column {
    class ViewModel: ObservableObject{
        @Binding var model: ColumnModel
        @Published var tasks: [TaskModel] = []
        @Published var sheetOpened = false
        var tasksInProgress: [TaskModel] {
            return tasks.filter { task in
                task.state != .DONE
            }
        }
        var disposeBag = Set<AnyCancellable>()
        init(model: Binding<ColumnModel>){
            self._model = model
        }
        
        func loadTasks(){
            ColumnService.instance.getColumnTasks(columnId: model.id)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tasks load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { tasks in
                    DispatchQueue.main.async {
                        self.tasks = tasks
                    }
                }.store(in: &disposeBag)
        }
        static func generateExample() -> ViewModel{
            let viewModel = ViewModel(model: .constant(ColumnModel.example))
            viewModel.tasks = [
                TaskModel.generateTask(name: "Task with progress", hasDeadline: true, state: .IN_PROGRESS, hasSubtasks: true),
                TaskModel.generateTask(name: "Normal task")
            ]
            return viewModel
        }
    }
    
    class ExampleViewModel: ViewModel{
        init(){
            super.init(model: .constant(ColumnModel.example))
            self.tasks = [
                TaskModel.generateTask(name: "Task with progress", hasDeadline: true, state: .IN_PROGRESS, hasSubtasks: true),
                TaskModel.generateTask(name: "Normal task")
            ]
        }
        
        override func loadTasks() {
            
        }
    }
}


struct ProjectTasks_Previews: PreviewProvider {
    static var previews: some View {
        Column(viewModel: Column.ExampleViewModel())
    }
}
