//
//  ProjectDashboard.swift
//  tmr
//
//  Created by Tomáš Geržičák on 30.04.2021.
//

import SwiftUI

struct ProjectDashboard: View {
    var progressNodes = [
        ProgressCircleNode(color: Color.primaryColor, progress: 0.2),
        ProgressCircleNode(color: Color.secondaryColor, progress: 0.4),
    ]
    var body: some View {
        VStack(alignment: .leading,spacing: 0){
            HStack(alignment: .center){
                ProgressCircleCluster(nodes: progressNodes)
                    .frame(width: 150)
                VStack(alignment: .leading){
                    HStack{
                        Rectangle()
                            .fill(Color.primaryColor)
                            .frame(width: 10, height: 10, alignment: .center)
                        HStack(spacing: 0){
                            Text("40").boldText()
                            Text("/50 Tasks done")
                                .greyText()
                        }
                    }
                    HStack{
                        Rectangle()
                            .fill(Color.secondaryColor)
                            .frame(width: 10, height: 10, alignment: .center)
                        HStack(spacing: 0){
                            Text("1:34")
                                .boldText()
                            Text("/50 Time spent")
                                .greyText()
                        }
                    }
                }
            }
            Spacer()
        }
    }
}

private extension Text {
    func boldText() -> Text {
        self.bold()
            .font(.system(size: 22))
    }
    
    func greyText() -> Text {
        self.font(.callout)
            .foregroundColor(.tmrGrey)
            .font(.system(size: 15))
    }
}

struct ProjectDashboard_Previews: PreviewProvider {
    static var previews: some View {
        ProjectDashboard()
    }
}
