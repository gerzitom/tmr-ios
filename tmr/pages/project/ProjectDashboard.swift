//
//  ProjectDashboard.swift
//  tmr
//
//  Created by Tomáš Geržičák on 30.04.2021.
//

import SwiftUI
import Combine

struct ProjectDashboard: View {
    @StateObject var viewModel: ViewModel
    var progressNodes = [
        ProgressCircleNode(color: Color.primaryColor, progress: 0.2),
        ProgressCircleNode(color: Color.secondaryColor, progress: 0.4),
    ]
    var body: some View {
        VStack(alignment: .leading,spacing: 0){
            HStack(alignment: .center){
                ProgressCircleCluster(nodes: progressNodes)
                    .frame(width: 150)
                VStack(alignment: .leading){
                    HStack{
                        Rectangle()
                            .fill(Color.primaryColor)
                            .frame(width: 10, height: 10, alignment: .center)
                        HStack(spacing: 0){
                            Text("\(viewModel.totalDoneTasks)").boldText()
                            Text("/50 Tasks done")
                                .greyText()
                        }
                    }
                    HStack{
                        Rectangle()
                            .fill(Color.secondaryColor)
                            .frame(width: 10, height: 10, alignment: .center)
                        HStack(spacing: 0){
                            Text("1:34")
                                .boldText()
                        }
                    }
                }
            }
            Spacer()
        }
        .onAppear{
            viewModel.loadTasks()
        }
    }
}

extension ProjectDashboard {
    class ViewModel:ObservableObject {
        var project: ProjectModel
        var totalDoneTasks: Int = 0
        @Published var tasks: [TaskModel] = []{
            didSet {
//                debugPrint(tasks)
                self.totalDoneTasks = tasks.reduce(0) { res, task in
                    if task.state != .DONE {
                        return res + 1
                    } else {
                        return res
                    }
                }
            }
        }
        var disposeBag = Set<AnyCancellable>()
        
        
        
        init(project: ProjectModel){
            self.project = project
        }
        
        
        func loadTasks(){
            ColumnService.instance.getColumnTasks(columnId: project.id)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("Tasks load finished")
                        break
                    case .failure(let err):
                        debugPrint(err)
                        break
                    }
                } receiveValue: { tasks in
                    DispatchQueue.main.async {
                        debugPrint(tasks)
                        self.tasks = tasks
                    }
                }.store(in: &disposeBag)
        }
    }
}

private extension Text {
    func boldText() -> Text {
        self.bold()
            .font(.system(size: 22))
    }
    
    func greyText() -> Text {
        self.font(.callout)
            .foregroundColor(.tmrGrey)
            .font(.system(size: 15))
    }
}

struct ProjectDashboard_Previews: PreviewProvider {
    static var previews: some View {
        ProjectDashboard(viewModel: ProjectDashboard.ViewModel(project: ProjectModel.example))
    }
}
