//
//  LoginPage.swift
//  tmr
//
//  Created by Tomáš Geržičák on 11.05.2021.
//

import SwiftUI
import Combine
import UserNotifications

struct LoginPage: View {
    @StateObject var viewModel: ViewModel = ViewModel()
    @StateObject private var notificationManager = NotificationManager()
    var body: some View {
        VStack(alignment: .leading){
            Text("Login to TMR")
                .font(.system(size: 34, weight: .bold, design: .default))
                .foregroundColor(.primaryColor)
                .padding(.vertical)
            VStack{
                TextField("Username", text: $viewModel.username)
                    .autocapitalization(.none)
                SecureField("Password", text: $viewModel.password)
            }
            .padding(.bottom)
            Button(action: {
                viewModel.login()
            }, label: {
                if viewModel.state == .loading {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                } else {
                    Text("Login")
                }
            })
            .buttonStyle(PrimaryStyle(fullWidth: true))
            NavigationLink(
                destination: WebsocketTest(),
                label: {
                    Text("Websockets")
                })
            
            Button("Send notification"){
                let dateComponents = Calendar.current.dateComponents([.hour, .minute], from: Date())
                guard let hour = dateComponents.hour, let minute = dateComponents.minute else { return }
                notificationManager.createLocalNotification(title: "Title", hour: hour, minute: minute + 1) { error in
                    if error == nil {
                        debugPrint(error)
                    }
                }
//                withAnimation {
//                    self.notificationManager.sendNotification(title: "First notification", subtitle: "This is text of first notification", body: "This is body of the notification", launchIn: 5)
//                }
                
//                let content = UNMutableNotificationContent()
//                content.title = "Feed the cat"
//                content.subtitle = "It looks hungry"
//                content.sound = UNNotificationSound.default
//
//                // show this notification five seconds from now
//                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
//
//                // choose a random identifier
//                let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
//
//                // add our notification request
//                UNUserNotificationCenter.current().add(request)
            }
            
            NavigationLink(destination: Organizations(organizations: $viewModel.organizations), isActive: $viewModel.loggedIn){
                Text("")
            }.hidden()
            Spacer()
        }
        .padding(.horizontal)
        .alert(isPresented: $viewModel.error, content: {
            Alert(title: Text("Validation error"), message: Text("Username of password is wrong"))
        })
        .onAppear{
            
        }
        
    }
}

extension LoginPage {
    enum State: Equatable{
        case initial
        case loading
        case error(String)
    }
    class ViewModel: ObservableObject {
        @Published var username: String = "admin"
        @Published var password: String = "haha"
        @Published var loggedIn = false
        @Published var error = false
        @Published var organizations: [OrganizationModel] = []
        @Published var state: State = .initial
        
        init(){
            self.state = .initial
        }
        init(state: State){
            self.state = state
        }
        
        
        var disposeBag = Set<AnyCancellable>()
        
        func login(){
            self.state = .loading
            let loginResponse = UserService.instance.login(username: username, password: password)
            loginResponse.sink { (completion) in
                switch completion {
                case .finished:
                    print("Login finished")
                    break
                case .failure(let err):
                    self.error = true
                    debugPrint(err)
                    self.state = .error("Error")
                    break
                }
            } receiveValue: { (user) in
                OrganizationsService.instance.getByCurrentUser()
                    .sink { (completion) in
                        switch completion {
                        case .finished:
                            print("Organizations load finished")
                            break
                        case .failure(let err):
                            debugPrint(err)
                            self.error = true
                            self.state = .error(err.localizedDescription)
                            break
                        }
                    } receiveValue: { (organizations) in
                        DispatchQueue.main.async {
                            self.organizations = organizations
                            self.loggedIn = true
                        }
                    }.store(in: &self.disposeBag)
            }.store(in: &disposeBag)
        }
        
        
    }
}

struct LoginPage_Previews: PreviewProvider {
    
    static var previews: some View {
        LoginPage(viewModel: LoginPage.ViewModel(state: .initial))
        LoginPage(viewModel: LoginPage.ViewModel(state: .loading))
    }
}
