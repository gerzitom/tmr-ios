//
//  Organizations.swift
//  tmr
//
//  Created by Tomáš Geržičák on 11.05.2021.
//

import SwiftUI

struct Organizations: View {
    @Binding var organizations: [OrganizationModel]
    var body: some View {
        VStack{
            ForEach(organizations.indices){index in
                NavigationLink(
                    destination: Projects(organization: $organizations[index]),
                    label: {
                        Organization(organization: organizations[index])
                    })
            }
            .padding(.horizontal)
            .padding(.vertical, 5)
            Spacer()
        }
        .navigationBarTitle("Choose organization", displayMode: .large)
    }
}

struct Organizations_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            Organizations(organizations: .constant(OrganizationModel.exampleArray))
        }
    }
}
