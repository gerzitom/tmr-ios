//
//  UserService.swift
//  tmr
//
//  Created by Tomáš Geržičák on 11.05.2021.
//

import Foundation
import Alamofire
import Combine

class UserService{
    
    typealias Entity = UserModel
    
    static let instance = UserService()
    var token: String = ""
    var authenticationHeaders: HTTPHeaders{
        ["Authorization": "Bearer \(token)"]
    }
    
    private var cachedUsers: [Int64:UserModel] = [:]
    private var loggedInUser: UserModel?
    
    private init(){}
    
    func login(username: String, password: String) -> Future<UserModel, Error>{
        struct LoginData:Codable {
            let username: String
            let password: String
        }
        let login = LoginData(username: username, password: password)
        
        return Future<UserModel, Error> { promise in
            AF.request(Constants.baseUrl + "/users/login",
                       method: .post,
                       parameters: login,
                       encoder: JSONParameterEncoder.default)
                .responseDecodable(of: TokenResponse.self){ response in
                    switch response.result {
                    case .success(let data):
                        self.token = data.token
                        AF.request(Constants.baseUrl + "/users/authenticateduser", headers: self.authenticationHeaders)
                            .responseDecodable(of: UserReadDto.self){ response in
                                switch response.result {
                                case .success(let data):
                                    self.loggedInUser = UserService.build(from: data)
                                    promise(.success(self.loggedInUser!))
                                    break
                                case .failure(let err):
                                    debugPrint(err)
                                    break
                                }
                            }
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func getLoggedInUser() -> UserModel {
        return self.loggedInUser!
    }
    
    func cacheUser(user: UserModel) {
        self.cachedUsers[user.id] = user
//        print(self.cachedUsers[user.id])
    }
    
    /// Potentional risk with new users
    func loadUsers(from: [Int64]) -> [UserModel]{
        var users:[UserModel] = []
        for userId in from {
            guard let user = self.cachedUsers[userId] else {
                continue
            }
            users.append(user)
        }
        return users
    }
    
    func loadUser(from: Int64) -> UserModel {
        return self.cachedUsers[from]!
    }
    
//    func loadUsersByProject(projectId: Int64) -> [UserModel]{
//        return self.cachedUsers.map({ (key: Int64, value: UserModel) -> UserModel in
//            return value
//        }).filter { userModel in
//            userModel.
//        }
//    }
    
    func getAll() -> UserModel {
        return UserModel.example
    }
    
    func add(newEntity: UserModel) -> Int64 {
        return 33
    }
    
    func get(id: Int64) -> Future<UserModel,Error> {
        return Future<UserModel, Error>{ promise in
            AF.request(Constants.baseUrl + "/users/\(id)", headers: self.authenticationHeaders)
                .responseDecodable(of: UserReadDto.self){ response in
                    switch response.result {
                    case .success(let userReadDto):
                        promise(.success(UserService.build(from: userReadDto)))
                    break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func update(id: Int64, entity: UserModel) {
        
    }
    
    static func build(from: UserReadDto) -> UserModel {
        return UserModel(id: from.id, username: from.username, name: from.name, avatar: from.avatar)
    }
    
    static func build(from: [UserReadDto]) -> [UserModel] {
        return from.map { (userReadDto) -> UserModel in
            return UserService.build(from: userReadDto)
        }
    }
    
}

struct TokenResponse: Decodable{
    let token: String
}

