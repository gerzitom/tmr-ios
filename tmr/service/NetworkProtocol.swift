//
//  NetworkProtocol.swift
//  tmr
//
//  Created by Tomáš Geržičák on 11.05.2021.
//

import Foundation
protocol NetworkProtocol {
    associatedtype Entity
    
    func getAll() -> Entity
    func add(newEntity: Entity) -> Int64
    func get(id: Int64) -> Entity
    func update(id: Int64, entity: Entity) -> Void
    
}


