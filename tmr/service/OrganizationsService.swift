//
//  OrganizationsService.swift
//  tmr
//
//  Created by Tomáš Geržičák on 11.05.2021.
//

import Foundation
import Combine
import Alamofire

class OrganizationsService: NetworkProtocol{
    typealias Entity = OrganizationModel
    
    
    static let instance = OrganizationsService()
    var cachedOrganizations : [Int64:OrganizationModel] = [:]
    
    private init(){}
    
    func getByCurrentUser() -> Future<[OrganizationModel], Error> {
        return Future<[OrganizationModel], Error>{ promise in
            AF.request(Constants.baseUrl + "/organizations", headers: UserService.instance.authenticationHeaders)
                .validate()
                .responseDecodable(of: [OrganizationReadDto].self){ response in
                    switch response.result {
                    case .success(let data):
//                        debugPrint(data)
                        let organizations: [OrganizationModel] = data.map { (readDto) -> OrganizationModel in
                            return OrganizationsService.build(from: readDto)
                        }
                        promise(.success(organizations))
                        break
                    case .failure(let err):
                        debugPrint(err)
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func getByUser(userId: Int64) -> Future<[OrganizationModel], Error>{
        return Future<[OrganizationModel], Error>{ promise in
            AF.request(Constants.baseUrl + "/organizations", headers: UserService.instance.authenticationHeaders)
                .validate()
                .responseDecodable(of: [OrganizationReadDto].self){ response in
                    switch response.result {
                    case .success(let data):
                        let organizations: [OrganizationModel] = data.map { (readDto) -> OrganizationModel in
                            return OrganizationsService.build(from: readDto)
                        }
                        promise(.success(organizations))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func getAll() -> OrganizationModel {
        return OrganizationModel.example
    }
    
    func add(newEntity: OrganizationModel) -> Int64 {
        return 34
    }
    
    func get(id: Int64) -> OrganizationModel {
        return OrganizationModel.example
    }
    
    func update(id: Int64, entity: OrganizationModel) {
        
    }
    
    func cacheOrganization(model: OrganizationModel){
        cachedOrganizations[model.id] = model
    }
    
    func loadOrganization(from: Int64) -> OrganizationModel?{
        return self.cachedOrganizations[from]
    }
    
    
    static func build(from: OrganizationReadDto) -> OrganizationModel {
        let users = UserService.build(from: from.users)
        users.forEach { (user) in
            UserService.instance.cacheUser(user: user)
        }
        let organizationModel = OrganizationModel(id: from.id, name: from.name, users: users, projects: ProjectService.build(from: from.projects))
        instance.cacheOrganization(model: organizationModel)
        return organizationModel
    }
    
    static func build(from: [OrganizationReadDto]) -> [OrganizationModel] {
        return from.map { (organizationReadDto) -> OrganizationModel in
            return OrganizationsService.build(from: organizationReadDto)
        }
    }
    
}
