//
//  TaskService.swift
//  tmr
//
//  Created by Tomáš Geržičák on 19.05.2021.
//

import Foundation
import Alamofire
import Combine

class TaskService {
    
    static var instance: TaskService = TaskService()
    private init(){}
    
    func add(dto: TaskDto) -> Future<Int64, Error>{
        return Future<Int64, Error>{ promise in
            let encoder = JSONParameterEncoder()
            let formatter = DateFormatter.yyyyMMdd
            encoder.encoder.dateEncodingStrategy = .formatted(formatter)
            AF.request(Constants.baseUrl + "/tasks",
                       method: .post,
                       parameters: dto,
                       encoder: encoder,
                       headers: UserService.instance.authenticationHeaders)
//                .responseData(completionHandler: { data in
//                    debugPrint(data)
//                })
                .responseDecodable(of: Int64.self){ response in
                    switch response.result {
                    case .success(let newTaskId):
                        promise(.success(newTaskId))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func edit(dto: TaskDto, taskId: Int64) -> Future<Void, Error>{
        return Future<Void, Error>{ promise in
            let encoder = JSONParameterEncoder()
            let formatter = DateFormatter.yyyyMMdd
            encoder.encoder.dateEncodingStrategy = .formatted(formatter)
            AF.request(Constants.baseUrl + "/tasks/\(taskId)",
                       method: .put,
                       parameters: dto,
                       encoder: encoder,
                       headers: UserService.instance.authenticationHeaders)
                .responseData(completionHandler: { data in
                    debugPrint(data)
                })
                .response{ response in
                    switch response.result {
                    case .success(_):
                        promise(.success(()))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func getComments(taskId: Int64) -> Future<[CommentModel], Error>{
        return Future<[CommentModel], Error>{ promise in
            AF.request(Constants.baseUrl + "/tasks/\(taskId)/comments", headers: UserService.instance.authenticationHeaders)
                .validate()
                .responseDecodable(of: [CommentReadDto].self){ response in
                    switch response.result {
                    case .success(let data):
                        let comments = TaskService.build(from: data)
                        debugPrint(comments)
                        promise(.success(comments))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func getComment(commentId: Int64) -> Future<CommentModel, Error>{
        return Future<CommentModel, Error>{ promise in
            AF.request(Constants.baseUrl + "/tasks/comments/\(commentId)", headers: UserService.instance.authenticationHeaders)
                .validate()
                .responseDecodable(of: CommentReadDto.self){ response in
                    switch response.result {
                    case .success(let data):
                        let comment = TaskService.build(from: data)
                        promise(.success(comment))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
//    func uploadPicture() -> Future<Int64, Err>{
//        let image = UIImage.init(named: "myImage")
//        let imgData = UIImageJPEGRepresentation(image!, 0.2)!
//
//         let parameters = ["name": rname] //Optional for extra parameter
//
//        Alamofire.upload(multipartFormData: { multipartFormData in
//                multipartFormData.append(imgData, withName: "fileset",fileName: "file.jpg", mimeType: "image/jpg")
//                for (key, value) in parameters {
//                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
//                    } //Optional for extra parameters
//            },
//        to:"mysite/upload.php")
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
//
//                upload.responseJSON { response in
//                     print(response.result.value)
//                }
//
//            case .failure(let encodingError):
//                print(encodingError)
//            }
//        }
//    }
    
    func addComment(dto: CommentDto, taskId: Int64) -> Future<Int64, Error>{
        return Future<Int64, Error>{ promise in
            let encoder = JSONParameterEncoder()
            let formatter = DateFormatter.iso
            encoder.encoder.dateEncodingStrategy = .formatted(formatter)
            AF.request(Constants.baseUrl + "/tasks/\(taskId)/comments",
                       method: .post,
                       parameters: dto,
                       encoder: encoder,
                       headers: UserService.instance.authenticationHeaders)
                .responseDecodable(of: Int64.self){ response in
                    switch response.result {
                    case .success(let newTaskId):
                        promise(.success(newTaskId))
                        break
                    case .failure(let err):
                        debugPrint(err)
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func startTrackTime(dto: TrackedTimeDto) -> Future<Int64, Error>{
        return Future<Int64, Error>{ promise in
            let encoder = JSONParameterEncoder()
            let formatter = DateFormatter.yyyyMMdd
            encoder.encoder.dateEncodingStrategy = .formatted(formatter)
            AF.request(Constants.baseUrl + "/trackedtime",
                       method: .post,
                       parameters: dto,
                       encoder: encoder,
                       headers: UserService.instance.authenticationHeaders)
                .responseDecodable(of: Int64.self){ response in
                    switch response.result {
                    case .success(let newTaskId):
                        promise(.success(newTaskId))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func endTrackTime(trackedTimeId: Int64) -> Future<Void, Error>{
        return Future<Void, Error>{ promise in
            let encoder = JSONParameterEncoder()
            let formatter = DateFormatter.yyyyMMdd
            encoder.encoder.dateEncodingStrategy = .formatted(formatter)
            AF.request(Constants.baseUrl + "/trackedtime/end/\(trackedTimeId)",
                       method: .put,
                       headers: UserService.instance.authenticationHeaders)
                .response(completionHandler: { response in
                    switch response.result {
                    case .success(_):
                        promise(.success(()))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                })
        }
    }
    
    func getTrackedTimes(taskId: Int64) -> Future<[TrackedTimeModel], Error>{
        return Future<[TrackedTimeModel], Error>{ promise in
            AF.request(Constants.baseUrl + "/trackedtime/\(taskId)", headers: UserService.instance.authenticationHeaders)
                .validate()
                .responseDecodable(of: [TrackedTimeReadDto].self){ response in
                    switch response.result {
                        case .success(let data):
                            let trackedTimes = TaskService.build(from: data)
                            promise(.success(trackedTimes))
                            break
                        case .failure(let err):
                            promise(.failure(err))
                            break
                    }
                }
        }
    }
    
    static func build(from: TrackedTimeReadDto) -> TrackedTimeModel{
        let diffComponents = Calendar.current.dateComponents([.second], from: from.start , to: from.end)
        let seconds = diffComponents.second ?? 0
        return TrackedTimeModel(user: UserService.instance.loadUser(from: from.userId), elapsedTime: seconds)
    }
    
    static func build(from: [TrackedTimeReadDto]) -> [TrackedTimeModel]{
        return from.map { commentReadDto in
            return build(from: commentReadDto)
        }
    }
    
    static func build(from: CommentReadDto) -> CommentModel{
        let user = UserService.instance.loadUser(from: from.userId)
        return CommentModel(id: from.id, text: from.text, user: user, created: from.created, image: from.image)
    }
    
    static func build(from: [CommentReadDto]) -> [CommentModel]{
        return from.map { commentReadDto in
            return build(from: commentReadDto)
        }
    }
    
    static func build(from: TaskReadDto) -> TaskModel{
//        return TaskModel(id: from.id, name: from.name, deadline: from.deadline, description: from.description, state: from.state, users: [], comments: [], subtasks: TaskService.build(from: from.subtasks), trackedTimes: [])
        let users = UserService.instance.loadUsers(from: from.users)
        return TaskModel(
            id: from.id,
            name: from.name,
            deadline: from.deadline,
            description: from.description,
            columnId: from.columnId,
            projectId: from.projectId,
            state: from.state,
            users: users,
            comments: [],
            subtasks: TaskService.build(from: from.subtasks),
            trackedTimes: [])
    }
    
    static func build(from: [TaskReadDto]) -> [TaskModel]{
        return from.map { taskReadDto in
            return build(from: taskReadDto)
        }
    }
}
