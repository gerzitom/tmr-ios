//
//  ProjectService.swift
//  tmr
//
//  Created by Tomáš Geržičák on 11.05.2021.
//

import Foundation
import Alamofire
import Combine

class ProjectService {
    
    static var instance: ProjectService = ProjectService()
    var cachedProjects: [Int64: ProjectModel] = [:]
    
    func getAll() -> ProjectModel {
//        AF.request(Constants.baseUrl + "/projects")
//            .validate()
//            .responseDecodable(of: <#T##Decodable.Protocol#>)
        return ProjectModel.example
    }
    
    func edit(dto: ProjectUpdateDto) -> Future<Int64, Error>{
        return Future<Int64, Error> { promise in
            let encoder = JSONParameterEncoder()
            let formatter = DateFormatter.yyyyMMdd
            encoder.encoder.dateEncodingStrategy = .formatted(formatter)
            AF.request(Constants.baseUrl + "/projects",
                       method: .put,
                       parameters: dto,
                       encoder: encoder,
                       headers: UserService.instance.authenticationHeaders)
                .validate()
                .responseJSON{ response in
                    switch response.result {
                    case .success(_):
                    break
                    case .failure(let err):
                        let errorMessage = try! JSONDecoder().decode(ErrorMessage.self, from: response.data!)
//                        debugPrint(response.response)
                        promise(.failure(errorMessage))
                        debugPrint(err)
                        break
                    }
                }
                .responseDecodable(of: Int64.self){ response in
                    switch response.result {
                    case .success(let newProjectId):
                        promise(.success(newProjectId))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func add(newEntity: ProjectDto) -> Future<Int64, Error>{
        return Future<Int64, Error> { promise in
            let encoder = JSONParameterEncoder()
            let formatter = DateFormatter.yyyyMMdd
            encoder.encoder.dateEncodingStrategy = .formatted(formatter)
            AF.request(Constants.baseUrl + "/projects",
                       method: .post,
                       parameters: newEntity,
                       encoder: encoder,
                       headers: UserService.instance.authenticationHeaders)
                .validate()
                .responseJSON{ response in
                    switch response.result {
                    case .success(_):
                    break
                    case .failure(let err):
                        let errorMessage = try! JSONDecoder().decode(ErrorMessage.self, from: response.data!)
//                        debugPrint(response.response)
                        promise(.failure(errorMessage))
                        debugPrint(err)
                        break
                    }
                }
                .responseDecodable(of: Int64.self){ response in
                    switch response.result {
                    case .success(let newProjectId):
                        promise(.success(newProjectId))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func get(id: Int64) -> ProjectModel {
        return ProjectModel.example
    }
    
    func update(id: Int64, entity: ProjectModel) {
        
    }
    
//    static func loadColumns(projectId: Int64) -> Future<[ColumnModel], Error>{
//        return Future<[ColumnModel],Error>{ promise in
//            AF.request(Constants.baseUrl + "/projects/\(projectId)/columns", headers: UserService.instance.authenticationHeaders)
//                .responseDecodable(of: [ColumnModel].self){ response in
//                    switch response.result {
//                    case .success(let data):
//                        promise(.success(data))
//                        break
//                    case .failure(let err):
//                        promise(.failure(err))
//                        break
//                    }
//                }
//        }
//    }
    
    func cache(model: ProjectModel){
        cachedProjects[model.id] = model
    }
    
    func load(from: Int64) -> ProjectModel?{
        return self.cachedProjects[from]
    }
    
    static func build(from: ProjectReadDto) -> ProjectModel {
        let project = ProjectModel(id: from.id, name: from.name, users: UserService.instance.loadUsers(from: from.users), organizationId: from.organizationId)
        instance.cache(model: project)
        return project
    }
    
    static func build(from: [ProjectReadDto]) -> [ProjectModel] {
        return from.map { (projectReadDto) -> ProjectModel in
            return ProjectService.build(from: projectReadDto)
        }
    }
    
    
    
    
}
