//
//  ColumnService.swift
//  tmr
//
//  Created by Tomáš Geržičák on 19.05.2021.
//

import Foundation
import Combine
import Alamofire

class ColumnService {
    
    static var instance:ColumnService = ColumnService()
    private init(){}
    
    static func build(from: ColumnReadDto) -> ColumnModel{
        return ColumnModel(id: from.id, name: from.name, order: from.order, projectId: from.projectId)
//        return ColumnModel(id: from.id, name: from.name, order: from.order, projectId: from.projectId, tasks: [])
    }
    
    static func build(from: [ColumnReadDto]) -> [ColumnModel]{
        return from.map { columnReadDto in
            return build(from: columnReadDto)
        }
    }
        
    
    func getByProjectId(id: Int64) -> Future<[ColumnModel], Error>{
        debugPrint("get by id")
        return Future<[ColumnModel], Error>{ promise in
            AF.request(Constants.baseUrl + "/projects/\(id)/columns", headers: UserService.instance.authenticationHeaders)
                .responseDecodable(of: [ColumnReadDto].self){ response in
                    switch response.result {
                    case .success(let data):
                        promise(.success(ColumnService.build(from: data)))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
    func getColumnTasks(columnId: Int64) -> Future<[TaskModel], Error>{
        return Future<[TaskModel], Error>{ promise in
            AF.request(Constants.baseUrl + "/columns/\(columnId)/tasks", headers: UserService.instance.authenticationHeaders)
                .responseDecodable(of: [TaskReadDto].self){ response in
                    switch response.result {
                    case .success(let data):
                        let tasks: [TaskModel] = TaskService.build(from: data)
                        promise(.success(tasks))
                        break
                    case .failure(let err):
                        promise(.failure(err))
                        break
                    }
                }
        }
    }
    
}
