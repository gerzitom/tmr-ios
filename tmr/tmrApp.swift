//
//  tmrApp.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI

@main
struct tmrApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView{
                LoginPage()
            }
        }
    }
}

public extension Color {
    static let primaryColor = Color("primaryColor")
    static let secondaryColor = Color("secondaryColor")
    static let tmrGrey = Color("tmrGrey")
    static let inactiveText = Color("inactiveText")
}

public extension Image {
    func data(url:URL?) -> Self {
        guard let url = url else {
            return self
        }
        if let data = try? Data(contentsOf: url) {
            return Image(uiImage: UIImage(data: data)!)
                .resizable()
        }
        return self
            .resizable()
    }
}

extension Date {
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
}

struct PrimaryStyle: ButtonStyle {
    var fullWidth = false
    func makeBody(configuration: Self.Configuration) -> some View {
        let result = configuration.label
            .font(.system(size: 21, weight: .bold, design: .default))
            .background(Color.primaryColor)
            .frame(maxWidth: fullWidth ? .infinity : nil)
        return result
            .padding(.vertical, 10)
            .padding(.horizontal, 30)
            .background(Color.primaryColor)
            .cornerRadius(20)
    }
}

struct SecondaryStyle: ButtonStyle {
    var fullWidth = false
    func makeBody(configuration: Self.Configuration) -> some View {
        let result = configuration.label
            .font(.system(size: 21, weight: .bold, design: .default))
//            .background(Color.black)
            .foregroundColor(.white)
            .frame(maxWidth: fullWidth ? .infinity : nil)
        return result
            .padding(.vertical, 10)
            .padding(.horizontal, 30)
            .background(Color.black)
            .cornerRadius(20)
    }
}


extension DateFormatter {
    static let yyyyMMdd: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    static let iso: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}
