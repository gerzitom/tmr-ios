//
//  Constants.swift
//  tmr
//
//  Created by Tomáš Geržičák on 11.05.2021.
//

import Foundation

class Constants {
    static let url = "https://agile-thicket-46384.herokuapp.com"
    //static let baseUrl = "https://agile-thicket-46384.herokuapp.com/rest"
    static let baseUrl = "http://localhost:8089/rest"
}
