//
//  ImageModel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 22.05.2021.
//

import Foundation
struct ImageModel:Codable {
    let url: String
    let description: String
    var fullUrl: String {
        Constants.baseUrl + url
    }
    
    init(_ url: String) {
        self.url = url
        self.description = ""
    }
}
