//
//  ImageModel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 22.05.2021.
//

import Foundation

protocol ImageProtocol: Decodable {
    var fullUrl: String { get }
    var description: String { get }
}

//extension ImageProtocol {
//    init?(from json: String) throws {
//        guard let data = try json.data(using: .utf8) else { return nil }
//        guard let value = try? JSONDecoder().decode(Self.self, from: data) else { return nil }
//        self = value
//    }
//}

struct ImageModel:Codable, ImageProtocol {
    let url: String
    let description: String
    var fullUrl: String {
        Constants.baseUrl + url
    }
    
    init(_ url: String) {
        self.url = url
        self.description = ""
    }
    
    enum Keys: CodingKey{
        case url, description
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        self.url = try container.decode(String.self, forKey: .url)
        self.description = try container.decode(String.self, forKey: .description)
    }
}

struct ForeignImageModel: ImageProtocol {
    var fullUrl: String
    var description: String
    init(_ url: String) {
        self.fullUrl = url
        self.description = ""
    }
    
    enum Keys: CodingKey{
        case url, description
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        self.fullUrl = try container.decode(String.self, forKey: .url)
        self.description = try container.decode(String.self, forKey: .description)
    }
}
