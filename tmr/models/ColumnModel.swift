//
//  ColumnModel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 16.05.2021.
//

import Foundation
struct ColumnModel:Identifiable{
    let id: Int64
    let name: String
    let order: Int
    let projectId: Int64
}

struct ColumnReadDto: Decodable{
    let id: Int64
    let name: String
    let order: Int
    let projectId: Int64
}

extension ColumnModel{
    static let example = ColumnModel(id: 33, name: "In progress", order: 1, projectId: 2)
    static let exampleArray = [
        ColumnModel(id: 33, name: "In progress", order: 1, projectId: 2),
        ColumnModel(id: 34, name: "Done", order: 2, projectId: 2)
    ]
}
