//
//  User.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import Foundation


struct UserModel: Identifiable {
    let id: Int64
    let username: String
    let name: String
    let avatar: ImageProtocol?
}

struct UserReadDto: Identifiable, Decodable {
    
    let id: Int64
    let username: String
    let name: String
    let avatar: ImageProtocol?
    
    enum Keys: CodingKey{
        case id, username, name, avatar
    }
    
    internal init(id: Int64, username: String, name: String, avatar: ImageProtocol?) {
        self.id = id
        self.username = username
        self.name = name
        self.avatar = avatar
    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        self.id = try container.decode(Int64.self, forKey: .id)
        self.username = try container.decode(String.self, forKey: .username)
        self.name = try container.decode(String.self, forKey: .name)
        self.avatar = try container.decode(ImageModel?.self, forKey: .avatar)
    }
}

struct UserDto{
    let username: String
    let name: String
}

extension UserModel{
    static var exampleArray = [
        UserModel(id: 34, username: "gerzitom", name: "Tomáš Geržičák", avatar: ForeignImageModel("https://www.lego.com/cdn/cs/set/assets/blt167d8e20620e4817/DC_-_Character_-_Details_-_Sidekick-Standard_-_Batman.jpg?fit=crop&format=jpg&quality=80&width=800&height=426&dpr=1")),
        UserModel(id: 45, username: "hubalmar", name: "Martin Hubal", avatar: nil)
    ]
    static var example = UserModel(id: 34, username: "gerzitom", name: "Tomáš Geržičák", avatar: ForeignImageModel("https://www.lego.com/cdn/cs/set/assets/blt167d8e20620e4817/DC_-_Character_-_Details_-_Sidekick-Standard_-_Batman.jpg?fit=crop&format=jpg&quality=80&width=800&height=426&dpr=1"))
}

extension UserReadDto {
    static var exampleUser = UserReadDto(id: 3, username: "gerzitom", name: "Tomáš Geržičák", avatar:ForeignImageModel("https://www.lego.com/cdn/cs/set/assets/blt167d8e20620e4817/DC_-_Character_-_Details_-_Sidekick-Standard_-_Batman.jpg?fit=crop&format=jpg&quality=80&width=800&height=426&dpr=1"))
    static var exampleArray = [
        UserReadDto(id: 3, username: "gerzitom", name: "Tomáš Geržičák", avatar: ForeignImageModel("https://www.lego.com/cdn/cs/set/assets/blt167d8e20620e4817/DC_-_Character_-_Details_-_Sidekick-Standard_-_Batman.jpg?fit=crop&format=jpg&quality=80&width=800&height=426&dpr=1")),
        UserReadDto(id: 4, username: "gerzitom", name: "Tomáš Geržičák", avatar: nil)
    ]
}
