//
//  User.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import Foundation


struct UserModel: Identifiable {
    let id: Int64
    let username: String
    let name: String
    let avatar: ImageModel?
}

struct UserReadDto: Identifiable, Decodable {
    let id: Int64
    let username: String
    let name: String
    let avatar: ImageModel?
}

struct UserDto{
    let username: String
    let name: String
}

extension UserModel{
    static var exampleArray = [
        UserModel(id: 34, username: "gerzitom", name: "Tomáš Geržičák", avatar: ImageModel("/images/avatar/JA.jpg")),
        UserModel(id: 45, username: "hubalmar", name: "Martin Hubal", avatar: nil)
    ]
    static var example = UserModel(id: 34, username: "gerzitom", name: "Tomáš Geržičák", avatar: ImageModel("/images/avatar/JA.jpg"))
}

extension UserReadDto {
    static var exampleUser = UserReadDto(id: 3, username: "gerzitom", name: "Tomáš Geržičák", avatar:ImageModel("/images/avatar/JA.jpg"))
    static var exampleArray = [
        UserReadDto(id: 3, username: "gerzitom", name: "Tomáš Geržičák", avatar: ImageModel("/images/avatar/JA.jpg")),
        UserReadDto(id: 4, username: "gerzitom", name: "Tomáš Geržičák", avatar: nil)
    ]
}
