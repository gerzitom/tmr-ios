//
//  CommentModel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 09.05.2021.
//

import Foundation

struct CommentModel: Identifiable{
    let id: Int64
    let text: String
    let user: UserModel
    let created: Date
}

struct CommentDto:Encodable {
    let text: String
    let user: Int64
    let created: Date
}

struct CommentReadDto:Decodable {
    let id: Int64
    let text: String
    let userId: Int64
    let created: Date
    
    private enum Keys: CodingKey{
        case id, text, created, userId
    }
    
    init(from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: Keys.self)
        self.id = try container.decode(Int64.self, forKey: .id)
        self.text = try container.decode(String.self, forKey: .text)
        self.userId = try container.decode(Int64.self, forKey: .userId)
//        self.created = try container.decode(Date.self, forKey: .created)
        let dateString = try container.decode(String.self, forKey: .created)
        
        let formatter = DateFormatter.iso
        if let date = formatter.date(from: dateString) {
            self.created = date
        } else {
            throw DecodingError.dataCorruptedError(forKey: .created,
                  in: container,
                  debugDescription: "Date string does not match format expected by formatter.")
        }
    }
}




extension CommentModel {
    static func generateComment() -> CommentModel{
        let texts = [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor et metus pulvinar dictum nunc. Urna risus lacinia purus ultricies quis libero. Nulla mi tincidunt platea ante orci. Nulla fermentum non mauris, morbi tellus lectus.Tincidunt placerat nec eu, ipsum faucibus lorem amet. Vitae elit viverra urna imperdiet. Turpis t viverra urna imperdiet.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor et metus pulvinar dictum nunc. Urna risus lacinia purus ultricies quis libero. Nulla mi tincidunt platea ante orci. Nulla fermentum non mauris, morbi tellus lectus.Tincidunt placerat nec eu, ipsum faucibus lorem amet. Vitae elit viverra urna imperdiet. Turpis t viverra urna imperdiet."
        ]
        return CommentModel(
            id: Int64.random(in: 1..<100),
            text: texts.randomElement()!,
            user: UserModel.example,
            created: Calendar.current.date(byAdding: .hour, value: Int.random(in: 0..<100), to: Date())!
        )
    }
}
