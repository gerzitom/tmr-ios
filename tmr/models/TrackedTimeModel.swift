//
//  TrackedTimeModel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 09.05.2021.
//

import Foundation
struct TrackedTimeModel: Identifiable{
    let id: UUID = UUID()
    let user: UserModel
    let elapsedTime: Int
}

struct TrackedTimeReadDto: Decodable{
    let id: Int64
    let taskId: Int64
    let userId: Int64
    let start: Date
    let end: Date
    
    enum Keys: CodingKey{
        case id, taskId, userId, start, end
    }
    init(from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: Keys.self)
        self.id = try container.decode(Int64.self, forKey: .id)
        self.taskId = try container.decode(Int64.self, forKey: .taskId)
        self.userId = try container.decode(Int64.self, forKey: .userId)
        let startDateString = try container.decode(String?.self, forKey: .start)
        if let unwrappedDateString = startDateString {
            let formatter = DateFormatter.iso
            if let date = formatter.date(from: unwrappedDateString) {
                self.start = date
            } else {
                throw DecodingError.dataCorruptedError(forKey: .start,
                      in: container,
                      debugDescription: "Date string does not match format expected by formatter.")
            }
        } else {
            self.start = Date()
        }
        
        let endDateString = try container.decode(String?.self, forKey: .end)
        if let unwrappedDateString = endDateString {
            let formatter = DateFormatter.iso
            if let date = formatter.date(from: unwrappedDateString) {
                self.end = date
            } else {
                throw DecodingError.dataCorruptedError(forKey: .end,
                      in: container,
                      debugDescription: "Date string does not match format expected by formatter.")
            }
        } else {
            self.end = Date()
        }
    }
}

struct TrackedTimeDto: Encodable {
    let userId: Int64
    let taskId: Int64
}

extension TrackedTimeModel {
    static let example = generate()
    static func generate() -> TrackedTimeModel {
        return TrackedTimeModel(user: UserModel.exampleArray.randomElement()!, elapsedTime: Int.random(in: 300..<3000))
    }
    static func generate(_ count: Int) -> [TrackedTimeModel]{
        if count < 1 {
            return [generate()]
        } else {
            var ret: [TrackedTimeModel] = []
            for _ in 0...count {
                ret.append(generate())
            }
            return ret
        }
    }
    
    static func formatTime(elapsedTime: Int) -> String{
        return "\(elapsedTime / 3600):\((elapsedTime % 3600) / 60):\((elapsedTime % 3600) % 60)"
    }
}
