//
//  Project.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import Foundation

struct ProjectModel: Identifiable {
    let id: Int64
    let name: String
    let users: [UserModel]
    let organizationId: Int64
    var columns: [ColumnModel] = []
}

struct ProjectReadDto: Identifiable, Decodable {
    let id: Int64
    let name: String
    let organizationId: Int64
    let users: [Int64]
}

struct ProjectDto: Codable {
    let name: String
    let organizationId: Int64
    let users: [Int64]
}

struct ProjectUpdateDto: Codable {
    let name: String
}

//extension ProjectReadDto

extension ProjectModel{
    static var example = ProjectModel(id: 34, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20))
    static var exampleArray = [
        ProjectModel(id: 34, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20)),
        ProjectModel(id: 35, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20)),
        ProjectModel(id: 36, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20)),
        ProjectModel(id: 24, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20)),
        ProjectModel(id: 25, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20)),
        ProjectModel(id: 26, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20)),
        ProjectModel(id: 27, name: "Project name", users: UserModel.exampleArray, organizationId: Int64.random(in: 0..<20))
    ]
}
