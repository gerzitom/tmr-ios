//
//  SprintModel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 05.05.2021.
//

import Foundation

struct SprintModel{
    let id: Int64
    let name: String
    let deadline: Date
    let tasks: [TaskModel]
}
