//
//  ErrorMessage.swift
//  tmr
//
//  Created by Tomáš Geržičák on 22.05.2021.
//

import Foundation
struct ErrorMessage: Decodable, Error {
    var message: String
}

extension ErrorMessage: LocalizedError {
    public var errorDescription: String? {
        return self.message
    }
}
