//
//  TaskModel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 05.05.2021.
//

import Foundation

enum TaskState:String, Decodable{
    case DONE
    case IN_PROGRESS
}

struct TaskModel:Identifiable {
    let id: Int64
    var name: String
    var deadline: Date?
    var description: String
    let columnId: Int64
    let projectId: Int64
    let state: TaskState
    var users: [UserModel]
    var comments: [CommentModel]
    let subtasks: [TaskModel]
    let trackedTimes: [TrackedTimeModel]
}

struct TaskDto:Codable {
    let name: String
    var deadline: Date?
    let description: String
    let projectId: Int64
    let columnId: Int64
    let users: [Int64]
}

struct TaskReadDto: Decodable{
    let id: Int64
    let name: String
    var deadline: Date?
    let description: String
    let columnId: Int64
    let projectId: Int64
    let subtasks: [TaskReadDto]
    let state: TaskState
    let users: [Int64]
    
//    let users: [Int64]
    enum Keys: CodingKey{
        case id, name, deadline, description, subtasks, comments, state, users, projectId, columnId
    }
    init(from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: Keys.self)
        self.id = try container.decode(Int64.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.description = try container.decode(String.self, forKey: .description)
        self.projectId = try container.decode(Int64.self, forKey: .projectId)
        self.columnId = try container.decode(Int64.self, forKey: .columnId)
        self.state = try container.decode(TaskState.self, forKey: .state)
        self.subtasks = try container.decode([TaskReadDto].self, forKey: .subtasks)
        self.users = try container.decode([Int64].self, forKey: .users)
        let dateString = try container.decode(String?.self, forKey: .deadline)
        if let unwrappedDateString = dateString {
            let formatter = DateFormatter.yyyyMMdd
            if let date = formatter.date(from: unwrappedDateString) {
                self.deadline = date
            } else {
                throw DecodingError.dataCorruptedError(forKey: .deadline,
                      in: container,
                      debugDescription: "Date string does not match format expected by formatter.")
            }
        } else {
            self.deadline = nil
        }
    }
}



extension TaskModel {
    static var example = generateTask(name: "Task name")
    static var exampleArray = [
        generateTask(name: "Task name", hasDeadline: false),
        generateTask(name: "Task name", hasDeadline: true, state: .DONE),
        generateTask(name: "Task name"),
        generateTask(name: "Task name", hasDeadline: true, state: .DONE),
        generateTask(name: "Task name", hasDeadline: false, state: .DONE),
    ]
    
    static func generateTask(name: String, hasDeadline: Bool = true, state: TaskState = .IN_PROGRESS, hasSubtasks: Bool = Bool.random()) -> TaskModel {
        var comments:[CommentModel] = []
        for _ in 0...Int.random(in: 4..<6){
            comments.append(CommentModel.generateComment())
        }
        var subtasks: [TaskModel] = []
        if hasSubtasks {
            for _ in 1...Int.random(in: 1..<4){
                subtasks.append(TaskModel.generateTask(name: "Subtask"))
            }
            for _ in 1...Int.random(in: 1..<4){
                subtasks.append(TaskModel.generateTask(name: "Subtask", hasDeadline: true, state: .DONE))
            }
        }
        return TaskModel(
            id: Int64.random(in: 1..<100),
            name: name,
//            deadline: nil,
            deadline: hasDeadline ? Calendar.current.date(byAdding: .day, value: Int.random(in: 1..<20), to: Date())! : nil,
            description: "HAHAHHAHAHHAHAAHHAHA",
            columnId: Int64.random(in: 0..<10),
            projectId: Int64.random(in: 0..<10),
            state: state,
            users: UserModel.exampleArray,
            comments: comments,
            subtasks: subtasks,
            trackedTimes: TrackedTimeModel.generate(2)
        )
    }
}
