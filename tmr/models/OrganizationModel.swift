//
//  Organization.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import Foundation

class OrganizationModel: Identifiable, ObservableObject {
    let id: Int64
    @Published var name: String
    @Published var users: [UserModel]
    @Published var projects = [ProjectModel]()
    init(id: Int64, name: String, users: [UserModel], projects: [ProjectModel]){
        self.id = id
        self.name = name
        self.users = users
        self.projects = projects
    }
}

struct OrganizationReadDto: Identifiable, Decodable{
    let id: Int64
    let name: String
    let users: [UserReadDto]
    let projects: [ProjectReadDto]
//    let founder: [UserReadDto]
}

extension OrganizationModel{
    static var example = OrganizationModel(id: 34, name: "Example organization", users: UserModel.exampleArray, projects: ProjectModel.exampleArray)
    
    static var exampleArray = [
        OrganizationModel(id: 34, name: "Example organization", users: UserModel.exampleArray, projects: ProjectModel.exampleArray),
        OrganizationModel(id: 43, name: "Other organization", users: UserModel.exampleArray, projects: ProjectModel.exampleArray)
    ]
}

extension OrganizationReadDto{
//    static var example = OrganizationReadDto(id: 34, name: "Example organization", users: UserReadDto.exampleArray, projects: ProjectReadDto)
}
