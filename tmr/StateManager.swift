//
//  StateManager.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import Foundation
class StateManager: ObservableObject{
    public static var instance: StateManager = StateManager()
    @Published var user: UserReadDto? = nil
    @Published var organizations: [OrganizationReadDto] = []
    
    private init(){}
}
