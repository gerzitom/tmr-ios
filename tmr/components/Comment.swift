//
//  Comment.swift
//  tmr
//
//  Created by Tomáš Geržičák on 09.05.2021.
//

import SwiftUI

struct Comment: View {
    var model: CommentModel
    var formatedDate: String {
        return model.created.getFormattedDate(format: "dd. MM. yy HH:mm")
    }
    var body: some View {
        HStack(alignment: .top){
            VStack{
                UserAvatar(user: model.user, size: .big)
                HStack{
                    Divider()
                }
            }
            VStack(alignment: .leading){
                HStack{
                    Text(model.user.name)
                        .font(.system(size: 20, weight: .semibold, design: .default))
                    Spacer()
                    Text(formatedDate)
                        .foregroundColor(.inactiveText)
                        .font(.system(size: 15, weight: .regular, design: .default))
                }
                .padding(.vertical, 9)
                Text(model.text)
                if model.image != nil {
                    Image("UserAvatarIcon")
                        .data(url: URL(string: model.image!.fullUrl))
                        .scaledToFit()
                }
            }
        }
    }
}

struct Comment_Previews: PreviewProvider {
    static var previews: some View {
        Comment(model: CommentModel.generateComment())
    }
}
