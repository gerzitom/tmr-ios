//
//  Carousel.swift
//  tmr
//
//  Created by Tomáš Geržičák on 06.05.2021.
//

import SwiftUI

struct Carousel: View {
    var pages: [CarouselPage]
    @State var index: Int = 0
    private var deviceWidth = UIScreen.main.bounds.width
    var offset: CGFloat{
        let newOffset = CGFloat(index) * self.deviceWidth
        return -newOffset
    }
    init(pages: [CarouselPage]){
        self.pages = pages
        index = pages[0].id
    }
    var body: some View {
        VStack(alignment: .leading){
            CarouselNav(pages: pages, index: $index)
            GeometryReader{ g in
                HStack(alignment: .top, spacing: 0){
                    ForEach(pages){ page in
                        page.content
                            .frame(width: deviceWidth)
                    }
                    
                }
                .offset(x: self.offset)
                .highPriorityGesture(DragGesture()
                    .onEnded({ (value) in
                        if value.translation.width > 50 {
                            // right
                            changeView(.right)
                        }
                        if -value.translation.width > 50 {
                            // left
                            changeView(.left)
                        }
                    })
                )
            }
        }
        .animation(.default)
        .edgesIgnoringSafeArea(.horizontal)
    }
    private enum ChangeDirection {
        case left
        case right
    }
    private func changeView(_ direction: ChangeDirection){
        switch direction {
        case .left:
            if self.index != self.pages.count - 1 {
                self.index += 1
            }
            break
        case .right:
            if self.index != 0 {
                self.index -= 1
            }
            break
        }
    }
}

struct CarouselNav: View {
    var pages: [CarouselPage]
    @Binding var index: Int
    var body: some View {
        ScrollView(.horizontal){
            ScrollViewReader{ (proxy: ScrollViewProxy) in
                HStack{
                    ForEach(pages){ page in
                        Button(action: {
                            self.index = page.id
                        }, label: {
                            Text("\(page.title)")
                                .font(.title)
                                .bold()
                                .foregroundColor(isActive(page: page) ? Color.primaryColor : Color.inactiveText)
                        })
                        .padding(.leading)
                        .padding(.trailing, 0)
                    }
                }
                .onChange(of: self.index, perform: { value in
                    withAnimation {
                        proxy.scrollTo(value, anchor: .topLeading)
                    }
                })
            }
        }
    }
    
    private func isActive(page: CarouselPage) -> Bool {
        return page.id == index
    }
}

struct CarouselScreens: View {
    @Binding var pages: [CarouselPage]
    var body: some View {
        Text("HAHA")
    }
}

struct CarouselPage: Identifiable  {
    let content: AnyView
    let title: String
    let id: Int
}

struct Carousel_Previews: PreviewProvider {
    static var previews: some View {
        let data = [
            CarouselPage(content: AnyView(Text("Dashboard")), title: "Dashboard", id: 0),
            CarouselPage(content: AnyView(Text("Sprints")), title: "Sprints", id: 1),
            CarouselPage(content: AnyView(Text("Tasks")), title: "Tasks", id: 2),
            CarouselPage(content: AnyView(Text("Sprints")), title: "Sprints", id: 3)
        ]
        Carousel(pages: data)
    }
}
