//
//  Organization.swift
//  tmr
//
//  Created by Tomáš Geržičák on 15.05.2021.
//

import SwiftUI

struct Organization: View {
    var organization: OrganizationModel
    var body: some View {
        VStack(alignment: .leading){
            Text("\(organization.name)")
                .font(.system(size: 22, weight: .bold, design: .default))
                .foregroundColor(.black)
            HStack {
                UserAvatars(users: organization.users, size: .small)
                Spacer()
                HStack{
                    Text("\(organization.projects.count)")
                        .foregroundColor(.primaryColor)
                        .bold()
                    Image(systemName: "briefcase")
                        .foregroundColor(.primaryColor)
                }
            }
        }
        .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .topLeading)
        .padding(.horizontal)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(10)
        .shadow(color: Color.black.opacity(0.3), radius: 7, x: 4.0, y: 4.0)
    }
}

struct Organization_Previews: PreviewProvider {
    static var previews: some View {
        Organization(organization: OrganizationModel.example)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
