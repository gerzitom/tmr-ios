//
//  UserTrackedTimeList.swift
//  tmr
//
//  Created by Tomáš Geržičák on 09.05.2021.
//

import SwiftUI

struct UserTrackedTimeList: View {
    var model: TrackedTimeModel
    var formatedTime: String {
        return TrackedTimeModel.formatTime(elapsedTime: model.elapsedTime)
    }
    var body: some View {
        HStack(alignment: .center, spacing: nil, content: {
            UserAvatar(user: model.user, size: .big)
            VStack(spacing: 2){
                HStack{
                    Text(model.user.name)
                        .font(.system(size: 19, weight: .semibold, design: .default))
                    Spacer()
                    VStack{
                        Text(formatedTime)
                            .font(.system(size: 19, weight: .semibold))
                    }
                }
//                HStack{
//                    Spacer()
//                    Text("9. 8. 2020 13:45 -- 9. 8.  2020 14:40")
//                        .font(.system(size: 12))
//                        .foregroundColor(.inactiveText)
//                }
            }
        })
    }
}

struct UserTrackedTimeList_Previews: PreviewProvider {
    static var previews: some View {
        List{
            UserTrackedTimeList(model: TrackedTimeModel.example)
        }
    }
}
