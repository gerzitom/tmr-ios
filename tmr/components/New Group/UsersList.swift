//
//  UsersList.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import SwiftUI

struct UsersList: View {
    var users: [UserModel]
    var addUserClicked: () -> Void
    var body: some View {
        VStack(alignment: .leading){
            ForEach(users){ user in
                UserListItem(user: user)
            }
            Button(action: {
                addUserClicked()
            }, label: {
                HStack{
                    Image(systemName: "plus.circle.fill")
                        .foregroundColor(.green)
                    Text("Add user")
                }
            })
            .padding(.top, users.count > 0 ? 10 : 0)
        }
    }
}

struct UsersList_Previews: PreviewProvider {
    static var previews: some View {
        UsersList(users: UserModel.exampleArray, addUserClicked: {})
    }
}
