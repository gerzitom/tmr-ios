//
//  UsersList.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import SwiftUI

struct UsersList: View {
    @Binding var users: [UserModel]
    var addUserClicked: () -> Void
    var userDeleted: (_ userIndex: Int64) -> Void
    var body: some View {
        VStack(alignment: .leading){
            List{
                Section(header: Text("Users")){
                    ForEach(users, id: \.id){ user in
                        UserListItem(user: user)
                    }.onDelete { (indexSet) in
                        let idsToDelete = indexSet.map { self.users[$0].id }
                        self.users.remove(atOffsets: indexSet)
                        userDeleted(idsToDelete[0])
                    }
                }
            }.listStyle(InsetGroupedListStyle())
            Button(action: {
                addUserClicked()
            }, label: {
                HStack{
                    Image(systemName: "plus.circle.fill")
                        .foregroundColor(.green)
                    Text("Add user")
                }
            })
            .padding(.top, users.count > 0 ? 10 : 0)
        }
    }
}

struct UsersListPreviewContainer: View {
    @State var isPresented = false
    @State var users = [
        UserModel.example,
        UserModel.example,
        UserModel.example
    ]
    var body: some View {
        UsersList(users: $users, addUserClicked: {
            
        }, userDeleted: { userIndex in
            
        })
            .sheet(isPresented: $isPresented, content: {
                ChooseUser(users: UserModel.exampleArray, userChoosed: { newUser in
                    
                }, sheetOpened: $isPresented)
            })
    }
}


struct UsersList_Previews: PreviewProvider {
    static var previews: some View {
        UsersListPreviewContainer()
    }
}
