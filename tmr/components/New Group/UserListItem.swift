//
//  UserListItem.swift
//  tmr
//
//  Created by Tomáš Geržičák on 29.04.2021.
//

import SwiftUI

struct UserListItem: View {
    var user: UserModel
    var body: some View {
        HStack(alignment: .center, spacing: nil, content: {
            UserAvatar(user: user, size: .big)
            Text(user.name)
                .font(.system(size: 17, weight: .semibold, design: .default))
        })
    }
}

struct UserListItem_Previews: PreviewProvider {
    static var previews: some View {
        List{
            UserListItem(user: UserModel.example)
        }
    }
}
