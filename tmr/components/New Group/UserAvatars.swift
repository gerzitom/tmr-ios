//
//  UserAvatars.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI

struct UserAvatars: View {
    var users: [UserModel]
    var size: UserAvatar.Size
    private var offset: CGFloat {
        switch size {
        case .big:
            return CGFloat(35)
        case .small:
            return CGFloat(23)
        }
    }
    var body: some View {
        HStack{
            Group{
                ForEach(users){ user in
                    UserAvatar(user: user, size: size)
                }
            }
            .padding(.trailing, offset * -1)
        }
        .padding(.trailing, offset)
    }
}

struct UserAvatars_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            UserAvatars(users: UserModel.exampleArray, size: .small)
            UserAvatars(users: UserModel.exampleArray, size: .big)
            
        }
    }
}
