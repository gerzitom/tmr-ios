//
//  UserAvatar.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI

struct UserAvatar: View {
    var user: UserModel
    var size: Size
    var nameShortcut: String {
        return user.name.components(separatedBy: " ").map { word in
            String(word.first!)
        }.reduce("", +)
    }
    private var settings: SizeSettings {
        switch size {
        case .big:
            return SizeSettings(size: 40, border: 3, whiteBorderSize: 6,textSize: 16)
        case .small:
            return SizeSettings(size: 20, border: 2, whiteBorderSize: 4, textSize: 7)
        }
    }
    var body: some View {
        VStack{
            if user.avatar != nil {
                Image("UserAvatarIcon")
                    .data(url: URL(string: user.avatar!.fullUrl))
                    .scaledToFill()
            } else {
                Text(nameShortcut)
                    .font(.system(size: settings.textSize))
            }
        }
        .frame(width: settings.size, height: settings.size, alignment: .center)
        .cornerRadius(50)
        .overlay(
            RoundedRectangle(cornerRadius: 60)
                .stroke(Color.primaryColor, lineWidth: settings.border)
        )
        .padding(.all, settings.whiteBorderSize)
        .background(Color.white)
        .cornerRadius(50)
    }
}

extension UserAvatar{
    struct SizeSettings{
        let size: CGFloat
        let border: CGFloat
        let whiteBorderSize: CGFloat
        let textSize: CGFloat
        
        init(size: Int, border: Int, whiteBorderSize: Int, textSize: Int){
            self.size = CGFloat(size)
            self.border = CGFloat(border)
            self.whiteBorderSize = CGFloat(whiteBorderSize)
            self.textSize = CGFloat(textSize)
        }
    }
    public enum Size {
        case small
        case big
    }
}

struct UserAvatar_Previews: PreviewProvider {
    static var previews: some View {
        UserAvatar(user: UserModel.example, size: .small)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
            .previewDisplayName("Small size")
        
        UserAvatar(user: UserModel.example, size: .big)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
            .previewDisplayName("Big size")
        VStack{
            UserAvatars(users: UserModel.exampleArray, size: .big)
            UserAvatars(users: UserModel.exampleArray, size: .small)
            List{
                ForEach(UserModel.exampleArray){ userModel in
                    UserListItem(user: userModel)
                }
            }
        }
    }
    
}
