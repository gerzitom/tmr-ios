//
//  Project.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI

struct Project: View{
    @StateObject var viewModel: ViewModel
    init(viewModel: ViewModel) {
        self._viewModel = StateObject(wrappedValue: viewModel)
    }
    var body: some View {
        VStack(alignment: .leading, spacing: 3){
            Text("\(viewModel.name)")
                .font(.system(size: 25))
                .foregroundColor(.black)
                .bold()
                .frame(maxWidth: .infinity, alignment: .leading)
            UserAvatars(users: viewModel.project.users, size: .small)
            ProgressLine(progress: viewModel.progress)
        }
        .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .topLeading)
        .padding(.horizontal)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(10)
        .shadow(color: Color.black.opacity(0.3), radius: 7, x: 4.0, y: 4.0)
    }
}

extension Project{
    class ViewModel:ObservableObject, Identifiable{
        @Published var name: String
        @Published var progress: Double
        var project: ProjectModel
        init(projectModel: ProjectModel) {
            self.project = projectModel
            self.name = projectModel.name
            self.progress = 0.2
        }
        
        public static func generateFull() -> ViewModel{
            let model = ViewModel(projectModel: ProjectModel.example)
            return model
        }
    }
}

struct Project_Previews: PreviewProvider {
    static var previews: some View {
        Project(viewModel: Project.ViewModel.generateFull())
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
