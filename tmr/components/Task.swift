//
//  Task.swift
//  tmr
//
//  Created by Tomáš Geržičák on 05.05.2021.
//

import SwiftUI

struct Task: View {
    var model: TaskModel
    private var name: String
    private var deadline: String {
        guard let unwarappedDeadline = model.deadline else {
            return ""
        }
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "d MMM"
        return formatter3.string(from: unwarappedDeadline)
    }
    private var isDeadlineCritical: Bool {
        guard let unwarappedDeadline = model.deadline else {
            return false
        }
        let tomorow = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        return unwarappedDeadline <= tomorow
    }
    private var progress: Double{
        if model.subtasks.count == 0 {
            return 0
        }
        let doneTasks = model.subtasks.filter { task in
            task.state == .DONE
        }
        
        return Double(doneTasks.count) / Double(model.subtasks.count)
    }
    
    private var state: String{
        switch model.state {
        case .DONE:
            return "Done"
        case .IN_PROGRESS:
            return "In progress"
        }
    }
    init(model: TaskModel){
        self.model = model
        self.name = model.name
//        self.progress = Double.random(in: 0.1..<0.9)
    }
    var body: some View {
        VStack(alignment: .leading, spacing: 4){
            HStack{
                Text("\(name)")
                    .font(.system(size: 22))
                    .foregroundColor(.black)
                    .bold()
                Spacer()
                HStack{
                    if (model.deadline != nil) {
                        Image(systemName: "clock")
                        Text("\(deadline)")
                        .bold()
                            .font(.system(size: 15))
                    }
                }
                .foregroundColor(isDeadlineCritical ? .red : .tmrGrey)
            }
            .padding(.bottom, 5)
            Text("\(state)")
                .foregroundColor(.black)
                .font(.caption)
            HStack{
                UserAvatars(users: model.users, size: .small)
                if model.comments.count > 0 {
                    HStack(spacing: 5){
                        Image(systemName: "message")
                        Text("\(model.comments.count)")
                            .font(.system(size: 15))
                    }
                    .foregroundColor(.primaryColor)
                }
            }
            if model.subtasks.count > 0 {
                ProgressLine(progress: progress)
            }
        }
        .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .topLeading)
        .padding(.horizontal)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(10)
        .shadow(color: Color.black.opacity(0.3), radius: 7, x: 4.0, y: 4.0)
    }
}

struct Task_Previews: PreviewProvider {
    static var previews: some View {
        Task(model: TaskModel.generateTask(name: "Create PWA"))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        Task(model: TaskModel.generateTask(name: "User models", hasDeadline: false))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
