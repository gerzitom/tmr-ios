//
//  ProgressBar.swift
//  tmr
//
//  Created by Tomáš Geržičák on 30.04.2021.
//

import SwiftUI

struct ProgressCircle: View {
    var progress: Float
    var color: Color
    var showText: Bool = true
    
    var body: some View {
        ZStack{
            Circle()
                .stroke(lineWidth: 15.0)
                .opacity(0.3)
                .foregroundColor(color)
                .scaledToFit()
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 15.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(color)
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.linear)
                .scaledToFit()
            if showText {
                Text(String(format: "%.0f %%", min(self.progress, 1.0)*100.0))
                    .font(.largeTitle)
                    .bold()
            }
        }
        .padding(.all)
    }
}

struct ProgressBar_Previews: PreviewProvider {
    static var previews: some View {
        ProgressCircle(progress: 0.3, color: Color.blue)
            .frame(width: 200)
    }
}
