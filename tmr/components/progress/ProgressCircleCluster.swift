//
//  ProgressCircleCluster.swift
//  tmr
//
//  Created by Tomáš Geržičák on 07.05.2021.
//

import SwiftUI

struct ProgressCircleCluster: View {
    @State var progress1: Float = 0.2
    var nodes: [ProgressCircleNode]
    var body: some View {
        ZStack(alignment: .top){
            ForEach(Array(nodes.enumerated()), id: \.offset){ i, node in
                ProgressCircle(progress: node.progress , color: node.color, showText: false)
                    .padding(CGFloat(i) * 20)
            }
        }
    }
}

struct ProgressCircleNode: Identifiable{
    let id: UUID = UUID()
    let color: Color
    let progress: Float
}

struct ProgressCircleCluster_Previews: PreviewProvider {
    static var previews: some View {
        let nodes = [
            ProgressCircleNode(color: Color.pink, progress: 0.2),
            ProgressCircleNode(color: Color.blue, progress: 0.4),
            ProgressCircleNode(color: Color.green, progress: 0.8),
            ProgressCircleNode(color: Color.purple, progress: 0.4)
        ]
        ProgressCircleCluster(nodes: nodes)
    }
}
