//
//  ProgressLine.swift
//  tmr
//
//  Created by Tomáš Geržičák on 28.04.2021.
//

import SwiftUI

struct ProgressLine: View {
    var progress: Double
    var displayProgress: String {
        let progressPercent = self.progress * 100
        return String(format: "%.0f", progressPercent)
    }
    var body: some View {
        VStack(spacing: 5){
            HStack{
                Text("PROGRESS")
                    .font(.system(size: 10, weight: Font.Weight.light, design: .default))
                    .foregroundColor(.black)
                Spacer()
                Text("\(displayProgress) %")
                    .bold()
                    .font(.system(size: 10))
                    .foregroundColor(.primaryColor)
            }
            ProgressView(value: progress)
                .progressViewStyle(LinearProgressViewStyle(tint: Color.primaryColor))
                .scaleEffect(x: 1, y: 1.5, anchor: .center)
        }
    }
}

struct ProgressLine_Previews: PreviewProvider {
    static var previews: some View {
        ProgressLine(progress: 0.5)
    }
}
